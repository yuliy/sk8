import datetime
import json
import select
import socket
import sys
import threading

import redis

# tornado imports
import tornado.websocket

import sockjs.tornado

from command import *
from player import *
from utility import *

from responses.commandresponse import *


class Client(sockjs.tornado.SockJSConnection):
    LOGIN_HELP = ("Type \"LOGIN\" followed by a username and password (ex. login sequoia password) to login." +
                  "\nType \"CREATE\" followed by a username and password (ex. create sequoia password) to create a user.")

    CREATE_COMMAND = "CREATE"
    LOGIN_COMMAND = "LOGIN"
    QUIT_COMMAND = "QUIT"

    CHAT_PREFIX = "/"

    def __init__(self, session):
        super(Client, self).__init__(session)

        # Register this client with the Game Server (oof this hack)
        self.router = self.session.server
        self.server = self.router.getGameServer()
        self.server.addClient(self)

        self.savesConfig = self.server.getRedisConfig("saves")
        self.redis = redis.Redis(self.savesConfig["host"], 
                                 self.savesConfig["port"],
                                 self.savesConfig["database"],
                                 self.savesConfig["password"])

        self.player = Player(self)

        self.server.printServerMessage("A client has connected.")

    def setupHeader( self ):
        header = ("      ___           ___           ___      \n"
                  "     /\  \         /\__\         /\  \     \n"
                  "    /::\  \       /:/  /        /::\  \    \n"
                  "   /:/\ \  \     /:/__/        /:/\:\  \   \n"
                  "  _\:\~\ \  \   /::\__\____   /::\~\:\  \  \n"
                  " /\ \:\ \ \__\ /:/\:::::\__\ /:/\:\ \:\__\ \n"
                  " \:\ \:\ \/__/ \/_|:|~~|~    \:\~\:\/:/  / \n"
                  "  \:\ \:\__\      |:|  |      \:\ \::/  /  \n"
                  "   \:\/:/  /      |:|  |       \:\/:/  /   \n"
                  "    \::/  /       |:|  |        \::/  /    \n"
                  "     \/__/         \|__|         \/__/     \n")
        header = OutputUtility.addHtmlTag(header, "coolcol")

        theader = ("      ___           ___           ___      \n"
                   "     /\__\         /\__\         /\  \     \n"
                   "    /::|  |       /:/  /        /::\  \    \n"
                   "   /:|:|  |      /:/  /        /:/\:\  \   \n"
                   "  /:/|:|__|__   /:/  /  ___   /:/  \:\__\  \n"
                   " /:/ |::::\__\ /:/__/  /\__\ /:/__/ \:|__| \n"
                   " \/__/~~/:/  / \:\  \ /:/  / \:\  \ /:/  / \n"
                   "       /:/  /   \:\  /:/  /   \:\  /:/  /  \n"
                   "      /:/  /     \:\/:/  /     \:\/:/  /   \n"
                   "     /:/  /       \::/  /       \::/__/    \n"
                   "     \/__/         \/__/         ~~        \n"
                   "\n\n")
        theader = OutputUtility.addHtmlTag(theader, "coolcol2")

        header += theader

        """
        toggle = False
        header = ""
        for line in theader.split('\n'):
            header += OutputUtility.addHtmlTag( line, "coolcol" if toggle else "coolcol2") + "\n"
            #header += OutputUtility.addHtmlTag( line, "skewing" if toggle else "skewing2") + "\n"
            toggle = not toggle
        """
        #header = OutputUtility.addHtmlTag( theader, "coolcol")

        header = header.replace(" ", "&nbsp;")

        width = 43
        subtitle = "Arcane Kids Moneypit Solutions"
        for i in range( 0, width/2 - len( subtitle ) / 2 ):
            header += "&nbsp;"
        header += subtitle + "\n"

        subtitle = "www.arcanekids.com/business"
        for i in range( 0, width/2 - len( subtitle ) / 2 ):
            header += "&nbsp;"
        header += subtitle + "\n"

        subtitle = "(c) 2013-2014"
        for i in range( 0, width/2 - len( subtitle ) / 2 ):
            header += "&nbsp;"
        header += subtitle + "\n\n"

        subtitle = Client.LOGIN_HELP
        for i in range( 0, width/2 - len( subtitle ) / 2 ):
            header += "&nbsp;"
        header += subtitle + "\n\n"

        return header

    def getPlayer(self):
        return self.player

    def getPlayerName(self):
        player = self.getPlayer()
        if (player):
            if (player.isConnected()):
                return player.getUsername()

        return None

    def getPlayerLocation(self):
        player = self.getPlayer()
        if (player):
            if (player.isConnected()):
                return player.getCurrentLocation()

        return None

    def savePlayerData(self, data):
        player = self.getPlayer()
        if (player):
            key = self.server.getPlayerKey(player.getUsername())
            self.redis.set(key, data)

    def loadPlayerData(self, username):
        player = self.getPlayer()
        if (player):
            key = self.server.getPlayerKey(username)
            return self.redis.get(key)

    def playerInLocation(self, location):
        playerLocation = self.getPlayerLocation()
        return (playerLocation and 
            playerLocation.shortName == location.shortName)

    def clientLoggedIn(self):
        return self.player.isConnected()

    def processMessage(self, message):
        username = "a client"
        if (self.clientLoggedIn()):
            username = self.getPlayerName()

        self.server.printServerMessage("Received message from " + username + ": " + message.strip())
        player_response = self.parseMessageForCommand(message)
        self.handleCommandResponse(player_response)

    def parseMessageForCommand(self, message):
        # strip HTML tags off of the message
        message = HTMLUtility.stripTags(message)

        splitMessage = message.split()

        # ensure that the client entered some command
        if (len(splitMessage) <= 0):
            if (not self.clientLoggedIn()):
                return CommandResponse(self.player,
                                       self.server,
                                       CommandResponse.SEND_TO_PLAYER,
                                       "Please attempt to login.  " + Client.LOGIN_HELP,
                                       {})
            else:
                return CommandResponse(self.player,
                                       self.server,
                                       CommandResponse.SEND_TO_PLAYER,
                                       "Please enter a command.  type \"HELP\" for help.",
                                       {})

        if (not self.clientLoggedIn()):
            command = splitMessage[0].upper()
            arguments = message[len(command):].strip()

            if (command == Client.CREATE_COMMAND):
                return self.player.processCommand(self.server, "CREATE", "CREATE", arguments)
            elif (command == Client.LOGIN_COMMAND):
                return self.player.processCommand(self.server, "LOGIN", "LOGIN", arguments)
            elif (command == Client.QUIT_COMMAND):
                return self.player.processCommand(self.server, "LOGOUT", "LOGOUT", arguments)
            else:
                return CommandResponse(self.player,
                                       self.server,
                                       CommandResponse.SEND_TO_PLAYER,
                                       "Invalid Command.  " + Client.LOGIN_HELP,
                                       {})
        else:
            command = splitMessage[0].upper()
            arguments = message[len(command):].strip()

            # check if the message starts with the chat prefix, if it does use it as a SAY command
            if (message[0] == Client.CHAT_PREFIX):
                return self.player.processCommand(self.server, "SAY", "SAY", message[1:])
            else:
                # see if the command used is an alias
                originalCommand = self.player.getOriginalCommand(command)
                if (originalCommand):
                    return self.player.processCommand(self.server, originalCommand, command, arguments)
                else:
                    return self.player.processCommand(self.server, command, command, arguments)

    def handleCommandResponse(self, response):
        responseCode = response.getCode()
        responseMessage = response.getMessage()
        responseArguments = response.getArguments()

        if (responseCode == Command.SEND_TO_PLAYER):
            self.sendMessage(responseMessage, responseArguments)
        elif (responseCode == Command.SEND_TO_ALL):
            self.broadcastMessage(responseMessage, responseArguments, 'message', False, True)
        elif (responseCode == Command.LOGOUT):
            self.logout()

    def encodeMessage(self, message, arguments=None):
        encodedMessage = message
        if (arguments):
            if (("override" in arguments and not arguments["override"] == "Y") or (not "override" in arguments)):
                encodedMessage = message
        else:
            encodedMessage = message
        
        encodedMessage = encodedMessage.replace("\n", "<br />") + "\n"

        return encodedMessage

    def sendMessage(self, message, arguments):
        json_message = {}
        json_message["message"] = self.encodeMessage(message, arguments)
        if (self.clientLoggedIn()):
            json_message["stats"] = self.encodeMessage(str(self.player))

            if (arguments):
                # if we got any additional arguments from the message processing pass those to the client
                for key in arguments:
                    json_message[key.lower()] = arguments[key]

                # if we see that this is a broadcast message then use the broadcast protocol
                if ("broadcast" in arguments):
                    self.broadcastMessage(self.encodeMessage(arguments["broadcast"]), {}, 'broadcast', True, False)

        self.send(json.dumps(json_message))

    def broadcastMessage(self, message, arguments, messageType, ignoreSelf, ignoreLocation):
        location = self.getPlayerLocation()
        username = self.player.getUsername()
        toBroadcast = {}
        toBroadcast[messageType] = message

        if (arguments):
            # if we got any additional arguments from the message processing pass those to the client
            for key in arguments:
                toBroadcast[key.lower()] = arguments[key]

        targets = self.server.getClientsInLocation(None if ignoreLocation else location, username if ignoreSelf else None) 
        self.broadcast(targets, json.dumps(toBroadcast))

    def logout(self):
        self.close()

    def on_open(self, request):
        self.sendMessage(self.setupHeader(), {})
      
    def on_message(self, message):
        self.processMessage(message)

    def on_close(self):
        if (self.clientLoggedIn()): # user is logged in
            self.server.printServerMessage("User " + self.getPlayerName() + " has disconnected.")
            self.player.save()
        else:
            self.server.printServerMessage("A client has disconnected.")

        self.server.removeClient(self) # remove client from server
