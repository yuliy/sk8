from utility import *

class RelativeLocationData:
    def __init__( self, exit ):
        # required parameters
        self.direction = exit['direction'].upper()
        self.shortName = exit['locationIndex']

        # optional parameters
        self.preposition = JSONUtility.getData('preposition', exit, "into")

        self.useTowards = ( True if ( JSONUtility.getData('towards', exit, "N") == "Y" ) else False )
        self.useThrough = ( True if ( JSONUtility.getData('through', exit, "N") == "Y" ) else False )
        self.useOn = ( True if ( JSONUtility.getData('on', exit, "N") == "Y" ) else False )

        self.canAir = ( True if ( JSONUtility.getData('air', exit, "N") == "Y" ) else False )
        self.canGrind = ( True if ( JSONUtility.getData('grind', exit, "N") == "Y" ) else False )

    def getPrintName( self ):
        return OutputUtility.addHtmlTag(self.name, "flashing")

    def getUseTowards( self ):
        return self.useTowards

    def getUseThrough( self ):
        return self.useThrough

    def getUseOn( self ):
        return self.useOn

    def getCanAir( self ):
        return self.canAir

    def getCanGrind( self ):
        return self.canGrind

    def getPrepositionFailureMessage( self, preposition, searchBy ):
        if ( preposition == "THROUGH" ):
            if ( self.getUseThrough() ):
                return None
            else:
                return "You cannot move through " + searchBy + ".  Maybe you want to move TOWARDS it?"
        elif ( preposition in [ "ON", "ONTO" ] ):
            if ( self.getUseOn() ):
                return None
            else:
                return "You cannot go on " + searchBy + ".  Maybe you want GO IN?"
        else:
            return None

    def hasTrickType( self, trickType ):
        if ( trickType == StatUtility.AIR_STAT ):
            return self.getCanAir()
        elif ( trickType == StatUtility.GRIND_STAT ):
            return self.getCanGrind()
        else:
            return False

class Location( object ):
    def __init__( self, world, shortName, locationData ):
        super( Location, self ).__init__()

        self.world = world

        # setup basic data
        self.shortName = shortName.upper()
        self.name = locationData['name'].upper()
        self.description = locationData['description']
        self.localDescription = locationData['localDescription']

        self.article = JSONUtility.getData( 'article', locationData )
        self.preposition = JSONUtility.getData( 'preposition', locationData )

        # setup entrance operations
        self.enterOperations = []
        enterOperationsData = JSONUtility.getData( 'onEnter', locationData, [] )
        for enterOperation in enterOperationsData:
            operationName = enterOperation['command']
            operationArguments = JSONUtility.getData( 'arguments', enterOperation )
            if ( not operationName == 'addargument' ):
                operationArguments = operationArguments.upper() if operationArguments else operationArguments
            
            self.enterOperations.append( ( operationName, operationArguments ) )

        # setup exits
        self.relativeLocations = {}
        exitData = JSONUtility.getData( 'exits', locationData, [] )
        for exit in exitData:
            relativeLocationData = RelativeLocationData( exit )
            self.relativeLocations[ relativeLocationData.direction ] = relativeLocationData

        self.entities = []

    def __str__( self ):
        article = "the"
        if ( self.article ):
            article = self.article

        preposition = "in"
        if ( self.preposition ):
            preposition = self.preposition

        currentLocationInfo = "You are currently " + preposition + " " + article + " " + self.getPrintName()
        if ( self.localDescription ):
            currentLocationInfo += ".  " + self.localDescription

        separator = "----------------------------------------------\n"
        if ( len( self.getEntities() ) > 0 ):
            currentLocationInfo += "\n" + separator + "In the area you see:\n"
            for entity in self.getEntities():
                currentLocationInfo += str( entity ) + "\n"

            currentLocationInfo = currentLocationInfo.strip() # strip off the last return carriage

        areaInfo = "\n" + separator
        for direction in self.relativeLocations:
            if ( self.relativeLocations[ direction ] ):
                relativeLocationData = self.relativeLocations[ direction ]
                relativeLocation = self.world.getLocationByShortName( relativeLocationData.shortName ) # find the actual location through shortName lookup

                areaInfo += ( "Towards the " if relativeLocationData.useTowards else "To the " ) + OutputUtility.addHtmlTag(direction, "flashing")
                article = "the"
                if ( relativeLocation.article ):
                    article = relativeLocation.article
                areaInfo += " you see " + article + " " + relativeLocation.getPrintName() + "\n"

        areaInfo += separator

        areaInfo = areaInfo.strip() # strip off the last return carriage

        return currentLocationInfo + ( "\n" + areaInfo if not areaInfo == "" else "" )

    def directionValid( self, direction ):
        directionValid = direction in self.relativeLocations.keys()
        return directionValid

    def getRelativeLocationByDirection( self, direction ):
        relativeLocationData = None
        if ( self.directionValid( direction ) ):
            relativeLocationData = self.relativeLocations[ direction ]

        if ( not relativeLocationData ):
            splitDirection = direction.split()
            if ( len( splitDirection ) > 0 ):
                if ( splitDirection[0] in GameUtility.ARTICLES ):
                    direction = ' '.join( direction.split()[1:] )
                    return self.getRelativeLocationByDirection( direction )

        return relativeLocationData

    def getRelativeLocationByName( self, name ):
        for direction in self.relativeLocations:
            relativeLocationData = self.relativeLocations[ direction ]
            location = self.world.getLocationByShortName( relativeLocationData.shortName )

            if ( location.getName() == name ):
                return relativeLocationData

        splitName = name.split()
        if ( len( splitName ) > 0 ):
            if ( splitName[0] in GameUtility.ARTICLES ):
                name = ' '.join( name.split()[1:] )
                return self.getRelativeLocationByName( name )

        return None

    def addEntity( self, entity ):
        self.entities.append( entity )

    def getShortName( self ):
        return self.shortName

    def getName( self ):
        return self.name

    def getPrintName( self ):
        return OutputUtility.addHtmlTag( self.getName(), "flashing" )

    def getEntities( self ):
        return self.entities

    def getEntityByType( self, entityType ):
        entityType = entityType.upper()
        for entity in self.entities:
            if ( entity.entityType == entityType ):
                return entity;

        return None

    def onEnter( self, player ):
        operationMessage = "\n"
        operationArguments = {}

        # perform all the on enter admin operations and take note of any returned arguments
        for operation in self.enterOperations:
            operationResponse = player.performAdminCommand( player.getServer(), operation[0], operation[1] )
            if ( operationResponse ):
                operationMessage += operationResponse.getMessage() + "\n"
                for key in operationResponse.getArguments():
                    operationArguments[ key ] = operationResponse.getArgument(key)

        return ( operationMessage, operationArguments )
