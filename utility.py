from HTMLParser import HTMLParser
import random

from xml.dom.minidom import *

class HTMLStripper( HTMLParser ):
    def __init__( self ):
        self.resetStripper()

    def resetStripper( self ):
        self.reset()
        self.fed = []

    def handle_data( self, d ):
        self.fed.append( d )

    def get_data(self):
        return ''.join( self.fed )

class ComparisonType():
    LESSTHAN = "lt"
    LESSTHANEQUAL = "lte"
    EQUAL = "eq"
    GREATERTHANEQUAL = "gte"
    GREATERTHAN = "gt"

class ColorMap():
    RED = "RED"
    GREEN = "GREEN"
    BLUE = "BLUE"
    WHITE = "WHITE"
    PLATINUM = "PLATINUM"

    colors = {}
    colors[ "RED" ] = "#FAAAAA"
    colors[ "GREEN" ] = "#AAFAAA"
    colors[ "BLUE" ] = "#AAAAFA"
    colors[ "WHITE" ] = "#FFFFFF"
    colors[ "PLATINUM"] = "#E5E4E2"

    @staticmethod
    def getColorHex( color ):
        colorHex = "#FFFFFF"
        if ( color in ColorMap.colors ):
            colorHex = ColorMap.colors[ color ]

        return colorHex

class XMLUtility( object ):
    @staticmethod
    def getInnerData( elementNode ):
        for child in elementNode.childNodes:
            return child.data

        return None

    @staticmethod
    def getNodeData( root, nodeId ):
        for node in root.getElementsByTagName( nodeId ):
            return XMLUtility.getInnerData( node )

        return None

class JSONUtility( object ):
    @staticmethod
    def getData( key, data, default=None ):
        if (key in data):
            return data[key]

        return default

class HTMLUtility():
    htmlStripper = None

    @staticmethod
    def stripTags( html ):
        if ( not HTMLUtility.htmlStripper ):
            HTMLUtility.htmlStripper = HTMLStripper()
        else:
            HTMLUtility.htmlStripper.resetStripper()
            
        HTMLUtility.htmlStripper.feed( html )
        return HTMLUtility.htmlStripper.get_data()

class GameUtility( object ):
    DEFAULT_DIE_SIDES = 20

    ARTICLES = [ 'THE', 'A', 'THAT', 'MY', 'YOUR', ]
    VERBS = [ 'BEEFED IT', 'FELL DOWN' ]
    PREPOSITIONS = [ 'THROUGH', 'TOWARDS', 'TO', 'IN', 'INTO', 'ON', 'ONTO', 'OUT', 'OVER' ]

    GHOST_ERROR = "You are a GHOST :*("

    @staticmethod
    def getWordArticle( word ):
        return "an" if word[0] in [ 'A', 'E', 'I', 'O', 'U' ] else "a"

    @staticmethod
    def rollDie( sides=20 ):
        return random.randint( 1, sides )

    @staticmethod
    def getRandomFailureVerb():
        return GameUtility.VERBS[ random.randint( 0, len( GameUtility.VERBS ) - 1 ) ]

    @staticmethod
    def separatePreposition( message ):
        words = message.split()
        preposition = None
        if ( len( words ) > 0 ):
            if ( words[0].upper() in GameUtility.PREPOSITIONS ):
                preposition = words[0].upper()
                message = ' '.join( words[1:] )

        return ( preposition, message )

class StatUtility( object ):
    AIR_STAT = 'AIR'
    GRIND_STAT = 'GRIND'

    STATS_WORDS = [ "GNARLY", "SICK", "NASTY", "WICKED", "ILL", "TIGHT", "RAD" ]
    BASE_STAT_WORD = "BOGUS"

    @staticmethod
    def addTags( word ):
        return OutputUtility.addHtmlTag(word, "skewing")

    @staticmethod
    def convertStatValue( stat ):
        if ( stat == 0 ):
            return StatUtility.BASE_STAT_WORD
        
        base = len( StatUtility.STATS_WORDS )
        statStr = ""
        
        while stat:
            statStr += StatUtility.STATS_WORDS[ stat % base ] + " "
            stat //= base
        
        return StatUtility.addTags( statStr.rstrip() )

    @staticmethod
    def getRandomModifier():
        return StatUtility.addTags( StatUtility.STATS_WORDS[ random.randint( 0, len( StatUtility.STATS_WORDS ) - 1 ) ] )


class OutputUtility():
    @staticmethod
    def appendPadding( message, padding, pad="&nbsp;" ):
        for i in range( 0, padding ):
            message += pad

        return message

    @staticmethod
    def addColorTag( value, color ):
        return "[COLOR " + color + "]" + value + "[/COLOR]"

    @staticmethod
    def addLinkTag( value, link ):
        return "[LINK " + link + "]" + value + "[/LINK]"

    @staticmethod
    def addMarqueeTag( value, behaviour, direction ):
        return "[MARQUEE " + behaviour + " " + direction + "]" + value + "[/MARQUEE]"

    @staticmethod
    def addFlashingTag( value ):
        return "[FLASHING]" + value + "[/FLASHING]"

    @staticmethod
    def addItalicsTag( value ):
        return "[ITALICS]" + value + "[/ITALICS]"

    @staticmethod
    def addHtmlTag( value, tag):
        return "<{0}>{1}</{0}>".format(tag,value)
