from utility import *

class ScriptTag( object ):
    OPEN_BRACKET_CODE = "["
    CLOSE_BRACKET_CODE = "]"

    def __init__( self, name, htmlName, preprocess=False ):
        super( ScriptTag, self ).__init__()

        self.name = name
        self.tailTag = ScriptTag.OPEN_BRACKET_CODE + "/" + self.name + ScriptTag.CLOSE_BRACKET_CODE
        self.htmlName = htmlName
        self.preprocess = preprocess

    def insertIntoLine( self, line, value, start, end ):
        head = line[ :start ]
        tail = line[ end: ]
        line = head + value + tail
        return line

    def handle( self, line ):
        headCheck = line.find( ScriptTag.OPEN_BRACKET_CODE + self.name )
        tailCheck = line.find( self.tailTag, headCheck )

        if ( not headCheck == -1 and not tailCheck == -1 ):
            headTagEnd = line.find( ScriptTag.CLOSE_BRACKET_CODE, headCheck ) + 1

            fullCommand = line[ headCheck:headTagEnd ]
            commandStripped = fullCommand[ 1:-1 ] # strip off the brackets

            commandSplit = commandStripped.split()

            return { "head": headCheck, "headEnd": headTagEnd, "tail": tailCheck, "split": commandSplit }

        return None

    def process( self, line, handle, player=None ):
        head = handle[ "head" ]
        headEnd = handle[ "headEnd" ]
        tail = handle[ "tail" ]
        arguments = handle[ "split" ]

        if ( len( arguments ) == 1 ):
            enclosing = line[ headEnd:tail ]

            full = "<{0}>{1}</{0}>".format( self.htmlName, enclosing )
            line = self.insertIntoLine( line, full, head, tail + len( self.tailTag ) )

            return self.apply( line, player )
        else:
            return line

    def apply( self, line, player=None ):
        handle = self.handle( line )
        if ( handle ):
            return self.process( line, handle, player )

        return line

class ColorTag( ScriptTag ):
    def process( self, line, handle, player=None ):
        head = handle[ "head" ]
        headEnd = handle[ "headEnd" ]
        tail = handle[ "tail" ]
        arguments = handle[ "split" ]

        if ( len( arguments ) == 2 ):
            color = arguments[1]
            colorHex = ColorMap.getColorHex( color )
            toColor = line[ headEnd:tail ]

            fullStyle = "<{0} style=\"color:{1};\">{2}</{0}>".format( self.htmlName, colorHex, toColor )
            line = self.insertIntoLine( line, fullStyle, head, tail + len( self.tailTag ) )

            return self.apply( line, player )
        else:
            return line

class LinkTag( ScriptTag ):
    def process( self, line, handle, player=None ):
        linkHeadCheck = handle[ "head" ]
        linkHeadTagEnd = handle[ "headEnd" ]
        linkTailCheck = handle[ "tail" ]
        commandSplit = handle[ "split" ]

        if ( len( commandSplit ) == 2 ):
            link = commandSplit[1]
            linkName = line[ linkHeadTagEnd:linkTailCheck ]

            fullLink = "<a href=\"" + link + "\" target=\"_blank\">" + linkName + "</a>"
            line = self.insertIntoLine( line, fullLink, linkHeadCheck, linkTailCheck + len( self.tailTag ) )

            return self.apply( line )
        elif ( len( commandSplit ) == 3 ):
            link = commandSplit[1]
            color = commandSplit[2]
            colorHex = ColorMap.getColorHex( color )
            linkName = line[ linkHeadTagEnd:linkTailCheck ]

            fullLink = "<a href=\"" + link + "\" target=\"_blank\" style=\"color:" + colorHex + "\">" + linkName + "</a>"
            line = self.insertIntoLine( line, fullLink, linkHeadCheck, linkTailCheck + len( self.tailTag ) )

            return self.apply( line, player )
        else:
            return line

class MarqueeTag( ScriptTag ):
    def process( self, line, handle, player=None ):
        head = handle[ "head" ]
        headEnd = handle[ "headEnd" ]
        tail = handle[ "tail" ]
        arguments = handle[ "split" ]

        if ( len( arguments ) == 3 ):
            behaviour = arguments[1]
            direction = arguments[2]
            toMarquee = line[ headEnd:tail ]

            fullMarquee = "<marquee behavior=\"" + behaviour + " direction=\"" + direction + ">" + toMarquee + "</marquee>"
            line = self.insertIntoLine( line, fullMarquee, head, tail + len( self.tailTag ) )

            return self.apply( line, player )

class CustomTag( ScriptTag ):
    def process( self, line, handle, player=None ):
        head = handle[ "head" ]
        headEnd = handle[ "headEnd" ]
        tail = handle[ "tail" ]
        arguments = handle[ "split" ]

        if ( len( arguments ) == 2 ):
            enclosing = line[ headEnd:tail ]

            full = "<{0}>{1}</{0}>".format( arguments[1], enclosing )
            line = self.insertIntoLine( line, full, head, tail + len( self.tailTag ) )

            return self.apply( line, player )
        else:
            return line

class RandomTextTag( ScriptTag ):
    def handle( self, line ):
        headCheck = line.find( ScriptTag.OPEN_BRACKET_CODE + self.name )

        if ( not headCheck == -1 ):
            headTagEnd = line.find( ScriptTag.CLOSE_BRACKET_CODE, headCheck ) + 1

            fullCommand = line[ headCheck:headTagEnd ]
            commandStripped = fullCommand[ 1:-1 ] # strip off the brackets

            commandSplit = commandStripped.split()

            return { "head": headCheck, "headEnd": headTagEnd, "split": commandSplit }

        return None

    def process( self, line, handle, player=None ):
        head = handle[ "head" ]
        headEnd = handle[ "headEnd" ]
        arguments = handle[ "split" ]

        if ( len( arguments ) == 2 and player ):
            listName = arguments[1]

            randomText = player.getWorld().getRandomDialog( listName )
            line = self.insertIntoLine( line, randomText, head, headEnd )

            return self.apply( line, player )

class ScriptingUtility():
    TAGS = None
    PREPROCESS_TAGS = []

    @staticmethod
    def addTag( tag ):
        if ( tag.preprocess ):
            ScriptingUtility.PREPROCESS_TAGS.append( tag.name )

        ScriptingUtility.TAGS[ tag.name ] = tag.apply

    @staticmethod
    def getTags():
        if ( ScriptingUtility.TAGS ):
            return ScriptingUtility.TAGS

        ScriptingUtility.TAGS = {}
        ScriptingUtility.addTag( ColorTag( "COLOR", "span", True ) )
        ScriptingUtility.addTag( LinkTag( "LINK", "a", True ) )
        ScriptingUtility.addTag( MarqueeTag( "MARQUEE", "marquee", True ) )
        ScriptingUtility.addTag( RandomTextTag( "RANDOM_TEXT", "", False ) )
        ScriptingUtility.addTag( ScriptTag( "ITALICS", "i", True ) )
        ScriptingUtility.addTag( CustomTag( "CUSTOM", "", True ) )

        return ScriptingUtility.TAGS

    @staticmethod
    def applyTags( line, player=None, tagList=None ):
        tags = ScriptingUtility.getTags()
        toApply = tags.keys()
        if ( tagList ):
            toApply = tagList

        for tag in toApply:
            if ( tag in tags ):
                line = tags[ tag ]( line, player )

        return line
