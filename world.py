import hashlib
import json
import random
import sys

from xml.dom.minidom import *

from entity import *
from location import *
from utility import *

from utilities.scriptingutility import *

class WorldBuilder( object ):
    def __init__( self ):
        super( WorldBuilder, self ).__init__()

    def buildFromLocationsFile( self, world, locationFile ):
        locations = {}

        locationsData = json.load( open( locationFile ) )

        for shortName in locationsData:
            locationData = locationsData[ shortName ]

            location = Location( world, shortName, locationData )

            entitiesData = locationData['entities'] if ('entities' in locationData) else []
            for entityData in entitiesData:
                entity = WorldEntity( entityData )
                location.addEntity( entity )

            locations[ shortName.upper() ] = location

        return locations

    def build( self, world, path ):
        locations = {}

        # parse the master location for the locations files we want to load into the world
        files = json.load( open( path ) )

        for fileName in files['location_files']:
            locationsFromFile = self.buildFromLocationsFile( world, fileName )

            for locationIndex in locationsFromFile:
                if ( not locationIndex in locations ):
                    locations[ locationIndex ] = locationsFromFile[ locationIndex ]

        return locations

class World(object):
    def __init__( self ):
        super( World, self ).__init__()

        self.builder = WorldBuilder()
        self.locations = self.builder.build( self, "_data/location/locations_master.json" )

        # load random dialog lists
        self.dialog = {}
        textXml = parse( "_data/location/text.xml" )
        listsNode = textXml.getElementsByTagName( 'lists' )
        if ( len( listsNode ) > 0 ):
            listsNode = listsNode[0]

        for listNode in listsNode.childNodes:
            if listNode.nodeType == xml.dom.Node.ELEMENT_NODE:
                listName = XMLUtility.getNodeData( listNode, "name" )

                lines = []
                linesNodes = listNode.getElementsByTagName( 'line' )
                for lineNode in linesNodes:
                    line = ScriptingUtility.applyTags( XMLUtility.getInnerData( lineNode ), None, ScriptingUtility.PREPROCESS_TAGS )
                    lines.append( line )

                self.dialog[ listName ] = lines

    def getLocationByIndex( self, index ):
        return self.locations[ index ]

    def getLocationIndex( self, location ):
        for i in range( 0, len( self.locations ) ):
            if ( self.locations[i].name == location ):
                return i

        return -1

    def getLocationByShortName( self, shortName ):
        return self.locations[ shortName.upper() ]

    def getLocationByName( self, name ):
        for location in self.locations:
            if ( self.locations[ location ].getName() == name ):
                return self.locations[ location ]

        splitName = name.split()
        if ( len( splitName ) > 0 ):
            if ( splitName[0] in GameUtility.ARTICLES ):
                name = ' '.join( name.split()[1:] )
                return self.getLocationByName( name )

        return None

    def findEntity( self, player, entityType ):
        currentLocation = player.getCurrentLocation()
        entity = currentLocation.getEntityByType( entityType )

        if ( not entity ):
            splitEntityType = entityType.split()
            if ( len( splitEntityType ) > 0 ):
                if ( splitEntityType[0] in GameUtility.ARTICLES ):
                    entityType = ' '.join( entityType.split()[1:] )
                    entity = currentLocation.getEntityByType( entityType )

        return entity

    def getRandomDialog( self, listName ):
        if listName in self.dialog:
            lines = self.dialog[ listName ]
            return lines[ random.randint( 0, len( lines ) - 1 ) ]

        return ""
