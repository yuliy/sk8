from utility import *

from utilities.scriptingutility import *

class Trick( object ):
    AIR_TYPE = "AIR"
    GRIND_TYPE = "GRIND"

    def __init__(self, trickName, trickData):
        super(Trick, self).__init__()

        self.name = trickName.upper()
        nameOverride = JSONUtility.getData('name', trickData)
        if (nameOverride):
            self.name = nameOverride.upper()

        self.trickType = JSONUtility.getData('type', trickData).upper()
        self.airRequired = int(JSONUtility.getData('airrequired', trickData))
        self.airUsed = int(JSONUtility.getData('airused', trickData))
        self.balance = int(JSONUtility.getData('balance', trickData))
        self.endState = JSONUtility.getData('endstate', trickData).upper()
        
        self.help = ScriptingUtility.applyTags(JSONUtility.getData('help', trickData), None, ScriptingUtility.PREPROCESS_TAGS )
        self.difficulty = int(JSONUtility.getData('difficulty', trickData))

    def getPrintName(self):
        color = "BLUE" if self.trickType == Trick.AIR_TYPE else "RED"
        return OutputUtility.addColorTag(self.name, color)

    def __str__(self):
        return self.getPrintName() + " ( {0} LVL {1} )".format(self.trickType, OutputUtility.addHtmlTag(str(self.difficulty), "flashing"))
