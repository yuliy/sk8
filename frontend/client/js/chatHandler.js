/**
 * ChatHandler Prototype constructor.  This will setup the SockJS connection object and initialize the connection
 * given some URL which has the server's websocket endpoint.  This constructor also sets up the jQuery on submit event
 * for submitting messages in the client to the server.
 *
 * @param url
 * @constructor
 */
var ChatHandler = function(url) {
    this.url = url;

    this.entities = {
        '<' : '<',
        '>' : '>',
        '&' : '&'
    };

    this.conn = null;

    this.connect();

    var self = this;
    $("#inputbox").submit(function(e) {
        e.preventDefault();

        // if we're connected
        if (self.conn !== null || self.conn.readyState == SockJS.OPEN) {
            var commandInput = $("#commandinput");

            self.conn.send(commandInput.val());
            var newMessage = commandInput.val().replace(/[<>&]/g, function (m) { return self.entities[m]; });
            newMessage = self._applyFlippingStyle(newMessage);

            var commandLog = $("#commandlog");
            commandLog.append('<li class="you">&nbsp;' + newMessage + '</li>');

            commandInput.val('');
        }
    });
};

/**
 * ChatHandler Prototype
 *
 * @type {{_applyFlippingStyle: Function, update_ui: Function, disconnect: Function, connect: Function}}
 */
ChatHandler.prototype = {
    /**
     * Utility function to apply the "flipping" tag to any text which matches the word "flip".
     *
     * @param message
     * @returns {XML|string|void}
     * @private
     */
    _applyFlippingStyle: function(message) {
        return message.replace(/(flip)/gi, "<flipping>$1</flipping>");
    },

    update_ui: function() {
        if (this.conn == null || this.conn.readyState != SockJS.OPEN) {
            $('#status').text('Disconnected');
            $('#stats').html('Disconnected');
        } else {
            $('#status').text('Connected');
        }
    },

    disconnect: function() {
        if (this.conn != null) {
            this.conn.close();
            this.conn = null;
            this.update_ui();
        }
    },

    connect: function() {
        this.disconnect();

        var conn = new SockJS(this.url);

        var self = this;

        conn.onopen = function() {
            self.update_ui();
        };

        conn.onmessage = function(event) {
            var message = JSON.parse(event.data);

            if (message['message']) {
                message['message'] = self._applyFlippingStyle(message['message']);
            }

            var log = $("#commandlog");

            if (!(/^\d+$/).test(message)) {
                if (message['message']) {
                    log.append('<li class="them">' + message['message'].replace(/[<>&]/g, function (m) { return self.entities[m]; }) + '</li>');
                }

                if (message['stats'])
                {
                    stats.innerHTML = message['stats'].replace(/[<>&]/g, function (m) { return self.entities[m]; })
                }

                if (message['broadcast'])
                {
                    var message = message['broadcast'].replace(/[<>&]/g, function (m) { return self.entities[m]; });
                    message = self._applyFlippingStyle(message);
                    chatlog.innerHTML = chatlog.innerHTML + '<li class="you">&nbsp;' + message + '</li>';
                    $('#chatlog').scrollTop($('#chatlog').prop("scrollHeight"));
                }

                if (message['broadcast-w'])
                {
                    var message = message['broadcast-w'].replace(/[<>&]/g, function (m) { return self.entities[m]; });
                    message = self._applyFlippingStyle(message);

                    chatlog.innerHTML = chatlog.innerHTML + '<li class="you">&nbsp;' + message + '</li>';
                    $('#chatlog').scrollTop($('#chatlog').prop("scrollHeight"));
                }
            } else {
                connected.innerHTML = message;
            }

            $('#commandlog').scrollTop($('#commandlog').prop("scrollHeight"));
        };

        conn.onclose = function() {
            self.conn = null;

            var log = $("#commandlog");

            log.append('<li class="them">' + "Successfully logged out.".replace(/[<>&]/g, function (m) { return self.entities[m]; }) + '</li>');
            $('#commandlog').scrollTop($('#commandlog').prop("scrollHeight"));

            self.update_ui();
        };

        this.conn = conn;
    }
};