from action import *
from entity import *
from utility import *

class Item ( WorldEntity ):
    def __init__( self, itemName, itemData ):
        super(Item, self).__init__(itemData)

        self.shortName = itemName

    def __str__( self ):
    	return self.entityType
        