import abc
import cgi

from responses.commandresponse import *

class Command ( object ):
    __metaclass__ = abc.ABCMeta

    NO_MESSAGE = -1
    SEND_TO_PLAYER = 1
    SEND_TO_ALL = 2
    LOGIN_SUCCESS = 3
    LOGIN_FAILURE = 4
    LOGOUT = 5
    ADMIN_RESPONSE = 6

    CANNOT_DO_THAT = "CANNOT DO TO THAT!"

    def __init__( self, name, helpMessage, equivalents=[], flags={} ):
        super( Command, self ).__init__()
        self.name = name.upper()
        self.equivalents = equivalents
        self.flags = flags
        self.buildHelpMessage( cgi.escape( helpMessage ) ) # escape special HTML characters in help message        

    def getName( self ):
        return self.name

    def getHelpMessage( self ):
        return self.helpMessage

    def getEquivalents( self ):
        return self.equivalents

    def getFlags( self ):
        return self.flags

    def buildHelpMessage( self, explanation ):
        self.helpMessage = self.name
        for equivalent in self.equivalents:
            self.helpMessage += "/" + equivalent

        self.helpMessage += " - " + explanation

    @abc.abstractmethod
    def run( self, player, server, arguments ):
        return CommandResponse( player, server, CommandResponse.NO_MESSAGE, "", {} )
        