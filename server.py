import datetime
import glob
import json
import os
import signal
import sys

# twitter imports
from twitter import Twitter, OAuth

# redis imports
import redis

# tornado imports
import tornado.httpserver
import tornado.ioloop
import tornado.web

import sockjs.tornado

from client import *
from world import *


class ChatRouter(sockjs.tornado.SockJSRouter):
    GAME_SERVER_EXCEPTION = ("Property 'game_server' is required" +
                             " in the 'user_settings' property.")

    def __init__(self,
                 connection,
                 prefix='',
                 user_settings=dict(),
                 io_loop=None):
        super(ChatRouter, self).__init__(connection, 
                                         prefix, 
                                         user_settings, 
                                         io_loop)

        # enforce the existence of the game_server property
        if 'game_server' not in self.settings:
            raise Exception(ChatRouter.GAME_SERVER_EXCEPTION)

    def getGameServer(self):
        return self.settings['game_server']


class Server:
    CLEANUP_FLAG = "--cleanup"
    ADMIN_FLAG = "--admin"

    PLAYER_KEY_PREFIX = "player"

    def __init__(self):
        self.port = 8008
        self.clients = []

        # load main configuration
        self.config = json.load(open("_data/config/main.json"))

        # load twitter configuration
        twitter_config = self.config["twitter"]
        oauth_config = twitter_config["oauth"]
        auth = OAuth(oauth_config["oauth_token"],
                     oauth_config["oauth_secret"],
                     oauth_config["consumer_key"],
                     oauth_config["consumer_secret"])
        self.twitterConnection = Twitter(auth=auth)

        # load redis configuration
        self.redisConfig = self.config["redis"]
        self.redisSavesConfig = self.redisConfig["saves"]
        self.redis = redis.Redis(self.redisSavesConfig["host"], 
                                 self.redisSavesConfig["port"],
                                 self.redisSavesConfig["database"],
                                 self.redisSavesConfig["password"])

        # setup required directories
        outputConfig = self.config["output"]
        if not os.path.exists(outputConfig["folder"]):
            os.makedirs(outputConfig["folder"])

        # check for the cleanup flag to clean the output directory of all output files
        if Server.CLEANUP_FLAG in sys.argv:
            if os.path.exists(outputConfig["folder"]):
                file_list = glob.glob(outputConfig["folder"] + "/*.txt")
                for fileName in file_list:
                    os.remove(fileName)

        # setup output stream file
        output_prefix = outputConfig["folder"] + "/" + outputConfig["prefix"]
        output_file = (output_prefix + "_" +
                       self.getDateString("%Y-%m-%d %H:%M") +
                       '.txt')
        self.outputStream = open(output_file, 'wb')

        # check if we are running this server in admin mode (allow deployment commands)
        self.is_admin_mode = False
        if Server.ADMIN_FLAG in sys.argv:
            self.is_admin_mode = True

        # initialize the World
        self.world = World()

        # setup handlers
        chat_router = ChatRouter(Client, '/ws', {'game_server': self})

        # setup tornado application
        self.application = tornado.web.Application(chat_router.urls)

    def getWorld(self):
        return self.world

    def getDateString(self, date_format):
        """
        Retrieve a formatted date string.

        :param date_format:
        :return:
        """
        return datetime.datetime.now().strftime(date_format)

    def get_is_admin_mode(self):
        return self.is_admin_mode

    def getRedisConfig(self, key=None):
        """
        Retrieve parts of the Redis configuration.  If the parameter Key is not specified or specified as None then
        the entire Redis configuration will be retrieved.

        :param key:
        :return:
        """
        if key:
            if key in self.redisConfig:
                return self.redisConfig[key]

            return None

        return self.redisConfig

    def getPlayerKey(self, username):
        """
        Retrieve the Redis key for storing Player information.

        :param username:
        :return:
        """
        return Server.PLAYER_KEY_PREFIX + ":" + username

    def buildWorld(self):
        """
        Build/Rebuild the World object attached to the server.

        :return:
        """
        self.world = World()
        self.printServerMessage("World rebuilt")

    def getTwitterConnection(self):
        """
        Retrieve the Twitter Connection object.

        :return:
        """
        return self.twitterConnection

    def printServerMessage(self, message):
        formatted_message = self.getDateString("%Y-%m-%d %H:%M:%S") + ": " + message
        print formatted_message
        self.outputStream.write(formatted_message + "\n")

    def closeOutputStream(self):
        self.outputStream.close()

    def addClient(self, client):
        """
        Add a Client to the list of currently active clients.

        :param client:
        :return:
        """
        self.clients.append(client)

    def removeClient(self, client):
        """
        Remove a Client from the list of currently active clients.

        :param client:
        :return:
        """
        if client in self.clients:
            self.clients.remove(client)

    def findClient(self, username):
        """
        Find a specific Client by searching all the clients for a particular username.
        :param username:
        :return:
        """
        for client in self.clients:
            if client.getPlayerName().lower() == username.lower():
                return client

        return None

    def findPlayer(self, username):
        """
        Find a specific Player by searching the clients for a particular username.

        :param username:
        :return:
        """
        client = self.findClient(username)
        if client:
            return client.getPlayer()

        return None

    def userExists(self, username):
        """
        Determine if a particular username already exists by checking if the player key for that username exists
        in Redis.

        :param username:
        :return:
        """
        key = self.getPlayerKey(username)
        return self.redis.exists(key)

    def usernameInUse(self, username):
        """
        Determine if a particular username/player is currently logged into the server.

        :param username:
        :return:
        """
        for client in self.clients:
            if client.getPlayerName() == username:
                return True

        return False

    def getClientsInLocation(self, location, to_ignore=None):
        in_location = []
        for client in self.clients:
            if location is None or client.playerInLocation(location):
                if not (client.getPlayerName() == to_ignore):
                    in_location.append(client)

        return in_location

    def getPlayersInLocation(self, location):
        clients_in_location = self.getClientsInLocation(location)
        users = "Players in " + location.getName() + ": "
        for client in clients_in_location:
            users += client.getPlayerName() + ", "

        return users.strip().strip(',')

    def sendToUsername(self, username, message, arguments):
        client = self.findClient(username)
        if client:
            client.sendMessage(message, arguments)
            return True

        return False

    def start(self):
        """
        Start up of the main execution loop for our Tornado server.

        :return:
        """
        self.printServerMessage("Server started on port " + str(self.port))

        http_server = tornado.httpserver.HTTPServer(self.application)
        http_server.listen(self.port)
        tornado.ioloop.IOLoop.instance().start()

if __name__ == "__main__":
    # Setup a handler for the SIGTERM signal that will be thrown if this application is running as a service and is
    # stopped.
    def sigterm_handler(_signo, _stack_frame):
        """
        Raise an exception on SIGTERM signal so that we can catch it in our try/finally block.

        :param _signo:
        :param _stack_frame:
        :exception SystemExit:
        :return:
        """
        raise SystemExit(0)

    signal.signal(signal.SIGTERM, sigterm_handler)

    # Setup the Server instance
    s = Server()

    try:
        s.start()
    finally:
        # If we receive an exception (for example if we receive the SIGTERM signal) then we need to close the file
        # output stream that is on the server so that the file contents will be properly flushed.
        s.closeOutputStream()
