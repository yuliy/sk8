from action import *
from utility import *
from trick import *

from utilities.scriptingutility import *

class WorldEntity(object):
    def __init__(self, entityData):
        super(WorldEntity, self).__init__()

        self.entityType = entityData['name'].upper()
        self.description = ScriptingUtility.applyTags(entityData['description'], None, ScriptingUtility.PREPROCESS_TAGS)

        self.useThrough = (True if (JSONUtility.getData('through', entityData, "N") == "Y") else False)
        self.article = JSONUtility.getData('article', entityData)
        self.tag = ""

        self.setupInteractionMessages(entityData)
        self.setupTrickInteractions(entityData)
        self.setupActions(entityData)

    def __str__(self):
        article = GameUtility.getWordArticle(self.entityType)

        # check to see if there is a configuration article override
        if (self.article):
            article = self.article

        # define the entity's difficulty string
        difficulty = ""
        if (len(self.trickDifficulties) > 0):
            difficulty += "( "

            for difficultyType in self.trickDifficulties:
                difficulty += difficultyType + " LVL " + str(self.trickDifficulties[difficultyType]) + ", "

            difficulty += "HEIGHT " + str(self.height) + "FT"
            difficulty += " )"

        return article + " " + OutputUtility.addColorTag(self.entityType, ColorMap.GREEN) + " " + difficulty

    def setupActions( self, data ):
        self.validActions = {}
        actions = JSONUtility.getData( 'actions', data, [] )
        for action in actions:
            self.validActions[ JSONUtility.getData('command', action ).upper() ] = Action( self, action )

    def setupInteractionMessages( self, data, messageTypes=['use', 'talk', 'read'] ):
        self.interactionMessages = {}
        for messageType in messageTypes:
            message = JSONUtility.getData( messageType, data )
            if ( message ):
                self.interactionMessages[ messageType.upper() ] = ScriptingUtility.applyTags( message, None, ScriptingUtility.PREPROCESS_TAGS )

    def setupTrickInteractions(self, data):
        self.height = JSONUtility.getData( 'height', data, 0 )
        self.trickDifficulties = {}
        difficulties = JSONUtility.getData( 'difficulties', data, {} )
        for difficulty in difficulties:
            self.trickDifficulties[difficulty.upper()] = difficulties[difficulty]

        self.trickModifiers = {}
        modifiers = JSONUtility.getData( 'modifiers', data, {} )
        for modifier in modifiers:
            self.trickModifiers[modifier.upper()] = modifiers[modifier]

    def getDescription( self ):
        fullDescription = self.description

        if ( not self.tag == "" ):
            fullDescription += "\nTAGGED: " + self.tag.upper()

        return fullDescription

    def getUseThrough( self ):
        return self.useThrough

    def getHeight( self ):
        return self.height

    def getAction( self, command ):
        if ( command in self.validActions ):
            return self.validActions[ command ]

        return None

    def hasInteractionMessage(self, interactionType):
        return interactionType in self.interactionMessages

    def getInteractionMessage( self, interactionType ):
        if (self.hasInteractionMessage(interactionType)):
            return self.interactionMessages[interactionType]

        return None

    def getTrickDifficulties( self ):
        return self.trickDifficulties

    def hasTrickDifficulty( self, trickType ):
        return trickType in self.trickDifficulties

    def getTrickDifficulty( self, trickType ):
        if ( self.hasTrickDifficulty( trickType ) ):
            return self.trickDifficulties[ trickType ]

        return None

    def getTrickModifiers( self ):
        return self.trickModifiers;

    def hasTrickModifier( self, trickType ):
        return trickType in self.trickModifiers

    def getTrickModifier( self, trickType ):
        if ( self.hasTrickModifier( trickType ) ):
            return self.trickModifiers[ trickType ]

        return None
    
    def isRamp(self):
        return ( self.hasTrickDifficulty( Trick.AIR_TYPE ) and self.hasTrickModifier( Trick.AIR_TYPE ) )
        
    def actionIsAllowed( self, action ):
        return ( action in self.validActions )
        
    def getPrepositionFailureMessage( self, preposition, trickName ):
        if ( preposition == "THROUGH" ):
            if ( self.getUseThrough() ):
                return None
            else:
                return "CANNOT " + trickName + " THROUGH THAT " + self.entityType + "."
        else:
            return None
