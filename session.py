from player import *
from responses.sessionresponse import *
from trick import *
from utility import *

class Session( object ):
    AIR_STATE = "AIR"
    GROUND_STATE = "GROUND"
    ON_STATE = "ON"

    SESSION_STARTED = "Session Started"
    SESSION_LAND = "Type End/Land to end the Session"
    SESSION_ENDED = "Session Over"

    SESSION_ERROR_INVALID_TRICK = "That is not a Trick that you can do!"
    SESSION_ERROR_OBJECT_NONEXISTENT = "That Entity does not exist!"
    SESSION_ERROR_ENTITY_REQUIRED = "That is not a Trick you can do with an Entity!"
    SESSION_ERROR_UNQIUE_ENTITY_REQUIRED = "You cannot use the same Entity twice!"
    SESSION_ERROR_STILL_IN_AIR = "You cannot Air off an Entity while in the Air!"

    SESSION_TRICK_OUTCOME_SUCCESS = "SUCCESS"
    SESSION_TRICK_OUTCOME_FAILURE = "FAILURE"
    SESSION_TRICK_OUTCOME_DISALLOWED = "DISALLOWED"

    SESSION_CANADVANCE_OUT_OF_TRICKS = "You are out of Tricks..."
    SESSION_CANADVANCE_OUT_OF_BALANCE = "You have used all of your Balance..."

    def __init__( self, player, simulation = False ):
        super( Session, self ).__init__()

        self.player = player

        self.simulation = simulation

        self.currentState = Session.GROUND_STATE
        self.currentHeight = 0
        self.currentBalance = player.getStat( StatUtility.GRIND_STAT )
        self.maxBalance = self.currentBalance

        self.entitiesUsed = []
        self.tricksUsed = []

        self.availableTricks = {}
        self.useableEntities = []

    def __str__( self ):
        # setup trick status
        separator = "    |    "
        status = "Current HEIGHT: " + str( self.currentHeight ) + "FT" + separator;
        status += "Current BALANCE: " + str( ( float( self.currentBalance ) / float( self.maxBalance ) ) * 100.0 ) + "%" + separator;
        status += "Current STATE: " + self.currentState + "\n\n";

        # get available tricks
        status += "Available Tricks: "
        usePadding = False
        padding = len( "Available Tricks: " )
        for available in self.availableTricks:
            trick = self.availableTricks[ available ]
            if ( usePadding ):
                status = OutputUtility.appendPadding( status, padding )

            usePadding = True
            status += str( trick ) + "\n"
        status += "\n"

        # get useable objects
        status += "Useable Entities: "
        usePadding = False
        padding = len( "Useable Entities: " )
        for useable in self.useableEntities:
            entity = self.player.getWorld().findEntity( self.player, useable )
            if ( usePadding ):
                status = OutputUtility.appendPadding( status, padding )

            usePadding = True
            status += str( entity ) + "\n"

        # get tricks used
        if ( len( self.tricksUsed ) > 0 ): # if we have even done any tricks yet
            combo = "\nCurrent Combo: "
            for trick in self.tricksUsed:
                combo += OutputUtility.addColorTag( trick, ColorMap.RED ) + " into "
            combo = combo[:-5]
            status += combo + "\n"

        # attach quit message
        status += "\n" + Session.SESSION_LAND;

        return status.strip()

    def getHeight( self ):
        return self.currentHeight

    def getBalace( self ):
        return currentBalance

    def getAvailableTricks( self ):
        return self.availableTricks

    def getUseableEntities( self ):
        return self.useableEntities

    def setHeight( self, value ):
        self.currentHeight = value

    def setBalance( self, value ):
        self.currentBalance = value

    def setSimulationMode( self, value ):
        self.simulation = value

    def useEntity( self, entity ):
        self.entitiesUsed.append( entity.entityType )

    def useTrick( self, trick ):
        self.tricksUsed.append( trick.name )

    def isEntityUseable( self, entity, player ):
        hasTrickDifficulties = len( entity.getTrickDifficulties() ) > 0
        entityNotUsed = ( not entity.entityType in self.entitiesUsed )
        # heightCheck = self.currentHeight >= entity.getHeight() or ( self.currentState == Session.GROUND_STATE and player.getStat( StatUtility.AIR_STAT ) > entity.getHeight() )
        
        heightCheck = False
        
        # if the entity is a ramp, height doesn't matter!
        if( self.currentState == Session.GROUND_STATE and entity.isRamp() ):
            heightCheck = True
        else:
            # determine the maximum height the player can reach
            possibleHeight = self.currentHeight
            
            # if the player is on the ground or grinding, add the height of their ollie
            if( self.currentState == Session.GROUND_STATE or self.currentState == Session.ON_STATE ):
                possibleHeight += player.getStat( StatUtility.AIR_STAT )
            
            heightCheck = possibleHeight >= entity.getHeight()
        
        return hasTrickDifficulties and entityNotUsed and heightCheck

    def findUseableEntities( self, player ):
        self.useableEntities = []
        for entity in player.getCurrentLocation().getEntities():
            entityUseable = self.isEntityUseable( entity, player )
            if ( entityUseable ):
                for trick in self.availableTricks:
                    if ( entity.hasTrickDifficulty( self.availableTricks[ trick ].trickType ) ):
                        self.useableEntities.append( entity.entityType )
                        break

    def findAvailableTricks( self, tricks, player ):
        unlockedTricks = player.getUnlockedTricks()

        self.availableTricks = {}
        for trickName in tricks:
            trick = tricks[ trickName ]

            if ( trickName in unlockedTricks ): # check if this trick has already been unlocked for the player
                if ( self.currentState == Session.GROUND_STATE or self.currentState == Session.ON_STATE ):
                    if ( trick.airRequired == 0 and trick.balance <= self.currentBalance ):
                        self.availableTricks[ trickName ] = trick
                elif ( self.currentState == Session.AIR_STATE ):
                    if ( trick.trickType == Trick.GRIND_TYPE ):
                        if ( trick.airRequired > 0 and self.currentHeight >= trick.airRequired and trick.balance <= self.currentBalance ):
                            self.availableTricks[ trickName ] = trick
                    elif ( trick.trickType == Trick.AIR_TYPE ):
                        if ( self.currentHeight >= trick.airRequired and trick.airUsed > 0 and self.currentHeight >= trick.airUsed ):
                            self.availableTricks[ trickName ] = trick

    def canAdvance( self ):
        if ( self.currentBalance <= 0 ):
            return ( False, Session.SESSION_CANADVANCE_OUT_OF_BALANCE )

        if ( len( self.getAvailableTricks() ) == 0 ):
            return ( False, Session.SESSION_CANADVANCE_OUT_OF_TRICKS )

        if ( len( self.getUseableEntities() ) == 0 ):
            for trick in self.getAvailableTricks():
                if self.getAvailableTricks()[ trick ].trickType == Trick.AIR_TYPE:
                    return ( True, "" )

            return ( False, Session.SESSION_CANADVANCE_OUT_OF_TRICKS )

        return ( True, "" )

    def processCommand( self, player, command, arguments ):
        trickName = command.strip()
        if ( trickName in self.availableTricks ):
            trick = self.availableTricks[ trickName ]
            if ( len( arguments.strip() ) == 0 ): # no entity associated with this trick
                if ( trick.trickType == Trick.AIR_TYPE ):
                    return self.doTrick( player, trick, None )
                else:
                    return SessionResponse( SessionResponse.SESSION_RESPONSE_STALL, Session.SESSION_ERROR_ENTITY_REQUIRED )
            else:
                # preprocess out some stuff
                splitArguments = arguments.split()
                preposition = None
                if ( len( splitArguments ) > 0 ):
                    if ( splitArguments[0].upper() in GameUtility.PREPOSITIONS ):
                        preposition = splitArguments[0].upper()
                        arguments = ' '.join( splitArguments[1:] )

                entity = player.getWorld().findEntity( player, arguments )
                relativeLocationDataByName = player.getCurrentLocation().getRelativeLocationByName( arguments )
                if ( entity ):
                    # do a preposition check
                    prepositionFailureMessage = entity.getPrepositionFailureMessage( preposition, trick.name )
                    if ( prepositionFailureMessage ):
                        return SessionResponse( SessionResponse.SESSION_RESPONSE_STALL, prepositionFailureMessage )

                    # make sure we can use this entity
                    if ( not entity.entityType in self.useableEntities ): 
                        return SessionResponse( SessionResponse.SESSION_RESPONSE_STALL, self.generateTrickResponse( trick, entity, "DISALLOWED" ) )
                    
                    # make sure this entity hasn't already been used
                    if ( entity.entityType in self.entitiesUsed ): 
                        return SessionResponse( SessionResponse.SESSION_RESPONSE_STALL, Session.SESSION_ERROR_UNQIUE_ENTITY_REQUIRED )
                    
                    # make sure that we can do this trick on this entity
                    if ( not entity.hasTrickDifficulty( trick.trickType ) ):
                        return SessionResponse( SessionResponse.SESSION_RESPONSE_STALL, self.generateTrickResponse( trick, entity, "DISALLOWED" ) )

                    # check for Actions on this entity
                    if ( entity.actionIsAllowed( command ) ):
                        action = entity.getAction( command )
                        return self.doTrickWithAction( player, trick, entity, action )
                    elif ( entity.actionIsAllowed( trick.trickType ) ):
                        action = entity.getAction( trick.trickType )
                        return self.doTrickWithAction( player, trick, entity, action )
                    else:
                        return self.doTrick( player, trick, entity )
                elif ( relativeLocationDataByName ):
                    # do a preposition checks
                    prepositionFailureMessage = relativeLocationDataByName.getPrepositionFailureMessage( preposition, arguments )
                    if ( prepositionFailureMessage ):
                        return SessionResponse( SessionResponse.SESSION_RESPONSE_STALL, prepositionFailureMessage )
                    
                    # make sure that we can do this trick on this location data
                    if ( not relativeLocationDataByName.hasTrickType( trick.trickType ) ):
                        return SessionResponse( SessionResponse.SESSION_RESPONSE_STALL, self.generateTrickResponse( trick, None, "DISALLOWED", arguments ) )

                    # do a trick and teleport using this location data
                    return self.doTrickWithTeleport( player, trick, relativeLocationDataByName )
                else:
                    return SessionResponse( SessionResponse.SESSION_RESPONSE_STALL, self.generateTrickResponse( trick, None, "DISALLOWED", None, arguments ) )
        else:
            return SessionResponse( SessionResponse.SESSION_RESPONSE_STALL, Session.SESSION_ERROR_INVALID_TRICK )

    def doTrickWithAction( self, player, trick, entity, action ):
        actionValid = action.validate(player)
        if ( actionValid[0] ):
            trickResponse = self.doTrick( player, trick, entity )

            if ( trickResponse.getCode() == SessionResponse.SESSION_RESPONSE_CONTINUE ):
                actionResponse = action.perform( player )
                actionResponseMessage = actionResponse.getMessage()
                
                if ( actionResponse.getCode() ):
                    # add the action response to our full response message
                    responseMessage = trickResponse.getMessage() + ( "\n" + actionResponseMessage if not actionResponseMessage.strip() == "" else "" )

                    trickResponse.setMessage( responseMessage )
                    trickResponse.addArguments( actionResponse.getArguments() )
                    return trickResponse
                else:
                    return SessionResponse( SessionResponse.SESSION_RESPONSE_FAILURE, actionResponse.getMessage(), actionResponse.getArguments() )
            else:
                return trickResponse
        else:
            actionResponse = action.perform( player )
            return SessionResponse( SessionResponse.SESSION_RESPONSE_FAILURE, actionResponse.getMessage(), actionResponse.getArguments() )

    def doTrickWithTeleport( self, player, trick, relativeLocationData ):
        trickResponse = self.doTrick( player, trick, None )

        if ( trickResponse.getCode() == SessionResponse.SESSION_RESPONSE_CONTINUE ):
            player.move( relativeLocationData.shortName )
            newLocation = player.getCurrentLocation()

            responseMessage = trickResponse.getMessage() + "\n" + self.generateMoveMessage( relativeLocationData, newLocation )
            return SessionResponse( SessionResponse.SESSION_RESPONSE_CONTINUE, responseMessage, { "broadcast": player.getUsername() + " has entered " + newLocation.name } )
        else:
            return trickResponse

    def doTrick( self, player, trick, entity ):
        if ( entity ):
            # do a stat check here
            statCheckResponse = self.statCheck( player, trick, entity )
            if ( not statCheckResponse[0] ): # if we failed the stat check
                return SessionResponse( SessionResponse.SESSION_RESPONSE_FAILURE, statCheckResponse[1] )
            
            if ( trick.trickType == Trick.AIR_TYPE ):
                if ( self.currentState == Session.GROUND_STATE ):
                    self.currentHeight = player.getStat( StatUtility.AIR_STAT ) # apply player's air stat to height
                    if ( entity.isRamp() ):
                        self.currentHeight += entity.height
                    entityAirModifier = entity.getTrickModifier( StatUtility.AIR_STAT )
                    if ( entityAirModifier ): # if the entity we aired off of has a air modifier, apply that too
                        self.currentHeight += entityAirModifier
                elif ( self.currentState == Session.ON_STATE ):
                    self.currentHeight += player.getStat( StatUtility.AIR_STAT )
                else:
                    self.currentHeight -= trick.airUsed

                self.useEntity( entity )
                self.useTrick( trick )
                self.currentState = trick.endState

                return SessionResponse( SessionResponse.SESSION_RESPONSE_CONTINUE, self.generateTrickResponse( trick, entity, "SUCCESS" ) + statCheckResponse[1] )
            elif ( trick.trickType == Trick.GRIND_TYPE ):
                self.currentHeight = entity.getHeight()
                self.currentBalance -= trick.balance
                self.useEntity( entity )
                self.useTrick( trick )
                self.currentState = trick.endState

                return SessionResponse( SessionResponse.SESSION_RESPONSE_CONTINUE, self.generateTrickResponse( trick, entity, "SUCCESS" ) + statCheckResponse[1] )
        else:
            # do a stat check here
            statCheckResponse = self.statCheck( player, trick, entity )
            if ( not statCheckResponse[0] ): # if we failed the stat check
                return SessionResponse( SessionResponse.SESSION_RESPONSE_FAILURE, statCheckResponse[1] )

            if ( trick.trickType == Trick.AIR_TYPE ):
                if ( self.currentState == Session.GROUND_STATE ):
                    self.currentHeight = player.getStat( StatUtility.AIR_STAT )
                elif ( self.currentState == Session.ON_STATE ):
                    self.currentHeight += player.getStat( StatUtility.AIR_STAT )
                else:
                    self.currentHeight -= trick.airUsed

            self.useTrick( trick )
            self.currentState = trick.endState

            return SessionResponse( SessionResponse.SESSION_RESPONSE_CONTINUE, self.generateTrickResponse( trick, None, "SUCCESS" ) + statCheckResponse[1] )

    def statCheck( self, player, trick, entity ):
        upgradeResult = False
        if ( not self.simulation ):
            check = player.statCheckTrick( trick.trickType, trick )
            if ( entity ):
                check = player.statCheckEntity( trick.trickType, entity )

            if ( not check ):
                damage = 1
                player.takeDamage( damage )
                damageMessage = self.generateDamangeResponse( player, damage )
                return ( False, self.generateTrickResponse( trick, entity, "FAILURE" ) + "\n" + damageMessage )
            else:
                upgradeResult = player.upgradeStat( trick.trickType, pow( 2, len( self.tricksUsed ) + 1 ) )

        upgradeResponse = self.generateUpgradeResponse( trick, player, upgradeResult )

        return ( True, upgradeResponse )

    def generateMoveMessage( self, relativeLocationData, newLocation ):
        useTowards = relativeLocationData.useTowards
        article = "the"
        if ( newLocation.article ):
            article = newLocation.article
        message = "Moved " + ( "towards the " if useTowards else "" ) +  relativeLocationData.direction
        message += " " + relativeLocationData.preposition + " " + article + " " + newLocation.name

        return message + "\n\n" + str( newLocation )

    def generateDamangeResponse( self, player, damage ):
        pointString = "points"
        if ( damage == 1 ):
            pointString = "point"

        health = player.getHealth()
        damageMessage = "You took " + str( damage ) + " " + pointString + " of DAMANGE.  Current HEALTH: " + str( health[ "current" ] ) + "/" + str( health[ "max" ] )
        return damageMessage + ( "\nYou have died and become a GHOST..." if player.isGhost() else "" )

    def generateUpgradeResponse( self, trick, player, outcome ):
        if ( outcome ):
            article = GameUtility.getWordArticle( trick.trickType )
            return "\n" + trick.trickType + " STAT INCREASED TO LEVEL " + str( player.getStat( trick.trickType ) ) + "!"
        else:
            return ""

    def generateTrickResponse( self, trick, entity, outcome, locationName=None, arguments=None ):
        if ( outcome == Session.SESSION_TRICK_OUTCOME_SUCCESS ): # if we did the trick successfully
            modifier = StatUtility.getRandomModifier()
            article = GameUtility.getWordArticle( modifier )

            if ( entity ):
                response = "You did " + article + " " + modifier + " " + trick.getPrintName()
                if ( self.currentState == Session.ON_STATE ):
                    return response + " on that " + entity.entityType
                elif ( self.currentState == Session.AIR_STATE ):
                    if ( entity.getUseThrough() ):
                        return response + " through that " + entity.entityType
                    #elif ( entity.hasTrickDifficulty( Trick.AIR_TYPE ) and entity.hasTrickModifier( Trick.AIR_TYPE ) ):
                    elif ( entity.isRamp() ):
                        return response + " off that " + entity.entityType
                    else:
                        return response + " over that " + entity.entityType
                else:
                    return "ERROR"
            else:
                return "You did " + article + " " + modifier + " " + trick.getPrintName()
        elif ( outcome == Session.SESSION_TRICK_OUTCOME_FAILURE ):
            verb = GameUtility.getRandomFailureVerb()
            return "You " + verb + " trying to do that " + trick.getPrintName()
        elif ( outcome == Session.SESSION_TRICK_OUTCOME_DISALLOWED ):
            if ( entity ):
                response = "You cannot " + trick.getPrintName()
                if ( trick.endState == Session.AIR_STATE ):
                    response += " over that " + entity.entityType
                else:
                    response += " on that " + entity.entityType

                if ( self.currentHeight < entity.getHeight() ):
                    response += ".  It is way too high up!"
                else:
                    response += "."

                return response
            else:
                if ( locationName ):
                    return "You cannot " + trick.getPrintName() + " to " + locationName + "!"
                elif ( arguments ):
                    return "Entity \"" + arguments + "\" does not exist!"
                else:
                    return "That entity does not exist!"

    def generateOneshotResponse( self, firstTrick, secondTrick, firstResponse, secondResponse, entity ):
        if ( secondResponse ): # if we got through the first trick successfully and got a second trick response
            firstResponseSplit = firstResponse.getMessage().split( "\n" )
            if ( len( firstResponseSplit ) > 0 ):
                firstResponseSplit = firstResponseSplit[1:]
            else:
                firstResponseSplit = []
            secondResponseSplit = secondResponse.getMessage().split( "\n" )
            if ( len( secondResponseSplit ) > 0 ):
                secondResponseSplit = secondResponseSplit[1:]
            else:
                secondResponseSplit = []

            firstModifier = StatUtility.getRandomModifier()
            firstArticle = GameUtility.getWordArticle( firstModifier )

            secondModifier = StatUtility.getRandomModifier()
            secondArticle = GameUtility.getWordArticle( secondModifier )

            response = "You did " + firstArticle + " " + firstModifier + " " + firstTrick.getPrintName() + " into " + secondArticle + " " + secondModifier + " " + secondTrick.name

            if ( entity ):
                if ( self.currentState == Session.ON_STATE ):
                    response += " on that " + entity.entityType
                elif ( self.currentState == Session.AIR_STATE ):
                    response += " over that " + entity.entityType

            for firstResponseLine in firstResponseSplit:
                response += "\n" + firstResponseLine

            for secondResponseLine in secondResponseSplit:
                response += "\n" + secondResponseLine

            return response
        else: # the first trick failed
            article = GameUtility.getWordArticle( firstTrick.name )
            return "You tried to do " + article + " " + firstTrick.name + " to setup that " + secondTrick.name + ", but " + firstResponse.getMessage()
