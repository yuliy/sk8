from utility import *

from utilities.scriptingutility import *

from responses.actionresponse import *

class Requirement( object ):
    ITEM_TYPE = "item"
    STAT_TYPE = "stat"
    FLAG_TYPE = "flag"

    def __init__( self, requirementData ):
        super( Requirement, self ).__init__()

        self.requirementType = JSONUtility.getData( 'type', requirementData )
        self.name = JSONUtility.getData( 'name', requirementData ).upper()
        self.quantity = JSONUtility.getData( 'quantity', requirementData )
        self.comparisonType = JSONUtility.getData( 'comparison', requirementData, ComparisonType.GREATERTHANEQUAL )

        if ( self.requirementType == Requirement.FLAG_TYPE ):
            self.comparisonType = ComparisonType.EQUAL

    def compare( self, value1, value2 ):
        if ( self.comparisonType == ComparisonType.LESSTHAN ):
            return ( value1 < value2 )
        elif ( self.comparisonType == ComparisonType.LESSTHANEQUAL ):
            return ( value1 <= value2 )
        elif ( self.comparisonType == ComparisonType.EQUAL ):
            return ( value1 == value2 )
        elif ( self.comparisonType == ComparisonType.GREATERTHANEQUAL ):
            return ( value1 >= value2 )
        elif ( self.comparisonType == ComparisonType.GREATERTHAN ):
            return ( value1 > value2 )
        else:
            return False;

    def validate( self, player ):
        if (self.requirementType == Requirement.ITEM_TYPE):
            if (player.hasItem(self.name)):
                return self.compare(player.getItemQuantity(self.name), self.quantity)
            else:
                if (self.quantity == 0 and self.comparisonType in [ComparisonType.LESSTHAN, ComparisonType.LESSTHANEQUAL]):
                    return True
                else:
                    return False
        elif (self.requirementType == Requirement.STAT_TYPE):
            if (player.hasStat(self.name)):
                return self.compare(player.getStat(self.name), self.quantity)
            else:
                return False
        elif (self.requirementType == Requirement.FLAG_TYPE):
            if (player.hasFlag(self.name)):
                return self.compare(player.getFlag(self.name), self.quantity)
            else:
                return False
        else:
            return False

class Action( object ):
    DEFAULT_SUCCESS_MESSAGE = "YOU DID THAT PERFECTLY!"
    DEFAULT_FAILURE_MESSAGE = "YOU FAILED TO DO THAT!"

    def __init__( self, entity, actionData ):
        super( Action, self ).__init__()

        self.entity = entity

        self.command = JSONUtility.getData('command', actionData)
        self.successMessage = JSONUtility.getData('successMessage', actionData)
        if ( self.successMessage ):
            ScriptingUtility.applyTags( self.successMessage, None, ScriptingUtility.PREPROCESS_TAGS )

        self.failureMessage = JSONUtility.getData('failureMessage', actionData)
        if ( self.failureMessage ):
            ScriptingUtility.applyTags( self.failureMessage, None, ScriptingUtility.PREPROCESS_TAGS )

        # setup requirements
        self.requirements = []
        requirementsData = JSONUtility.getData('requirements', actionData, [])
        for requirementData in requirementsData:
            self.requirements.append( Requirement( requirementData ) )

        self.successOperations = []
        self.failureOperations = []

        # setup success/failure actions
        successOperationsData = JSONUtility.getData('onSuccess', actionData, [])
        for successOperationData in successOperationsData:
            command = JSONUtility.getData('command', successOperationData)
            arguments = JSONUtility.getData('arguments', successOperationData)

            if ( not command in [ 'addargument', 'setplayerflag' ] and arguments and not isinstance(arguments, int) ):
                arguments = arguments.upper()

            self.successOperations.append( ( command, arguments ) )

        failureOperationsData = JSONUtility.getData('onFailure', actionData, [])
        for failureOperationData in failureOperationsData:
            command = JSONUtility.getData('command', failureOperationData)
            arguments = JSONUtility.getData('arguments', failureOperationData)

            if ( not command in [ 'addargument', 'setplayerflag' ] and arguments and not isinstance(arguments, int) ):
                arguments = arguments.upper()

            self.failureOperations.append( ( command, arguments ) )
            
    def perform( self, player ):
        response = self.attempt( player )
        if ( response[0] ):
            return self.generateResponse( player, response, ActionResponse.ACTION_RESPONSE_SUCCESS )
        else:
            if ( response[1] == "failed" ):
                return self.generateResponse( player, response, ActionResponse.ACTION_RESPONSE_FAILURE )
            else:
                return ActionResponse( ActionResponse.ACTION_RESPONSE_FAILURE, ActionResponse.NO_ITEM_FAILURE_MESSAGE, {} )

    def attempt( self, player ):
        playerdata = player.data
        actionValid = self.validate(player)

        if ( actionValid[0] ):
            itemsUsedResponse = ""

            for requirement in self.requirements:
                if ( requirement.requirementType == Requirement.ITEM_TYPE ):
                    if ( player.hasItem(requirement.name) and requirement.quantity > 0 ):
                        player.removeItem(requirement.name, requirement.quantity)

                        item = player.itemFactory.getItem(requirement.name.upper())
                        itemName = item.entityType
                        itemsUsedResponse += "USED " + str(requirement.quantity) + " OF YOUR " + itemName

                        if (player.getItemQuantity(requirement.name) == 0):
                            itemsUsedResponse += "\nYOU ARE OUT OF " + itemName + "!"

            return (1, itemsUsedResponse)
        else:
            return actionValid

    def validate( self, playerdata ):
        haveRequiredInventory = True
        haveRequiredStats = True
        haveRequiredFlags = True

        for requirement in self.requirements:
            if ( requirement.requirementType == Requirement.ITEM_TYPE ):
                haveRequiredInventory = requirement.validate( playerdata )
            elif ( requirement.requirementType == Requirement.STAT_TYPE ):
                haveRequiredStats = requirement.validate( playerdata )
            elif ( requirement.requirementType == Requirement.FLAG_TYPE ):
                haveRequiredFlags = requirement.validate( playerdata )

        if ( haveRequiredInventory and haveRequiredStats and haveRequiredFlags ):
            return ( 1, "allowed" )
        elif ( haveRequiredInventory ):
            return ( 0, "failed" )
        else:
            return ( 0, "disallowed" )

    def generateResponse( self, player, response, responseType ):
        operations = None
        if ( responseType == ActionResponse.ACTION_RESPONSE_FAILURE ):
            operations = self.failureOperations
        elif ( responseType == ActionResponse.ACTION_RESPONSE_SUCCESS ):
            operations = self.successOperations
        else:
            operations = []

        operationMessage = "\n"
        operationArguments = {}

        # perform all the action admin operations and take note of any returned arguments
        for operation in operations:
            operationResponse = player.performAdminCommand( player.getServer(), operation[0], operation[1] )
            if ( operationResponse ):
                operationMessage += operationResponse.getMessage() + "\n"
                for key in operationResponse.getArguments():
                    operationArguments[ key ] = operationResponse.getArgument( key )

        operationMessage = operationMessage.strip()
        typeResponse = None
        if ( responseType == ActionResponse.ACTION_RESPONSE_FAILURE ):
            typeResponse = self.failureMessage
        elif ( responseType == ActionResponse.ACTION_RESPONSE_SUCCESS ):
            typeResponse = self.successMessage

        # build the action response string
        if ( not typeResponse ):
            typeResponse = ""
            if ( len( response[1].strip() ) > 0 ):
                typeResponse += response[1]

            # if this is a success response check if there are any interaction messages we need to append
            if ( responseType == ActionResponse.ACTION_RESPONSE_SUCCESS ):
                interactionMessage = self.entity.getInteractionMessage( self.command )
                if ( interactionMessage ):
                    if ( len( typeResponse.strip() ) > 0 ):
                        typeResponse += "\n" + interactionMessage
                    else:
                        typeResponse += interactionMessage
        else:
            if ( len( response[1].strip() ) > 0 ):
                typeResponse = response[1] + "\n" + typeResponse

            # if this is a success response check if there are any interaction messages we need to append
            if ( responseType == ActionResponse.ACTION_RESPONSE_SUCCESS ):
                interactionMessage = self.entity.getInteractionMessage( self.command )
                if ( interactionMessage ):
                    typeResponse += "\n" + interactionMessage

        responseMessage = typeResponse + ( "\n\n" + operationMessage if len( operationMessage.strip() ) > 0 else "" )
        
        # do a final catch to make sure the message is populated
        if ( len( typeResponse.strip() ) <= 0 ): 
            defaultMessage = ""
            if ( responseType == ActionResponse.ACTION_RESPONSE_FAILURE ):
                defaultMessage = ActionResponse.DEFAULT_FAILURE_MESSAGE
            elif ( responseType == ActionResponse.ACTION_RESPONSE_SUCCESS ):
                defaultMessage = ActionResponse.DEFAULT_SUCCESS_MESSAGE

            responseMessage = defaultMessage + ( "\n\n" + operationMessage if len( operationMessage.strip() ) > 0 else "" )

        # build the response object
        aResponse = ActionResponse( responseType, responseMessage, operationArguments )
        return aResponse
