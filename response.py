class Response( object ):
    def __init__( self, code, message, arguments=None ):
        super( Response, self ).__init__()

        self.code = code
        self.message = message
        self.arguments = arguments
        if ( not self.arguments ):
            self.arguments = {}

    def getCode( self ):
        return self.code

    def getMessage( self ):
        return self.message

    def setMessage( self, message ):
        self.message = message

    def getArguments( self ):
        return self.arguments

    def getArgument( self, key ):
        if ( key in self.arguments ):
            return self.arguments[ key ]

        return None

    def setArgument( self, key, value ):
        if ( self.getArgument( key ) ):
            self.arguments[ key ] = value
            return True

        return False

    def addArgument( self, key, value ):
        if ( not key in self.arguments ):
            self.arguments[ key ] = value
            return True

        return False

    def addArguments( self, arguments ):
        for key in arguments:
            self.addArgument( key, arguments[ key ] )

    def combineArguments( self, response ):
        for key in response.getArguments():
            self.addArgument( key, response.getArgument( key ) )
            