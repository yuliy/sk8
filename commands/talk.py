from command import *
from responses.commandresponse import *

class Talk( Command ):
    def __init__( self ):
        super( Talk, self ).__init__( "Talk",
                                      "Talk to <SOMETHING>",
                                      [],
                                      { 'player': True } )

    def run( self, player, server, arguments ):
        if ( len( arguments.strip() ) == 0 ):
            return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, "You cannot talk to nothing!", {} )

        if( len( arguments.split() ) > 0 and arguments.split()[0].lower() == "to" ):
            arguments = ' '.join( arguments.split()[1:] )

        entity = player.getWorld().findEntity( player, arguments )

        if ( entity ):
            action = entity.getAction( "TALK" )
            if ( action ):
                actionResponse = entity.getAction( "TALK" ).perform( player )
                actionResponse.addArgument( "override", "Y" )
                return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, actionResponse.getMessage(), actionResponse.getArguments() )
            else:
                interactionMessage = entity.getInteractionMessage( "TALK" )
                responseMessage = interactionMessage

                if ( not responseMessage ):
                    responseMessage = "You cannot talk to that \"" + entity.entityType + "\""

                return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, responseMessage, { "override": "Y" } )
        else:
            return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, "There is no \"" + arguments + "\"...", {} )
            