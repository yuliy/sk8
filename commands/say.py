from command import *
from responses.commandresponse import *

class Say(Command):
    def __init__(self):
        super(Say, self).__init__("Say", 
                                  "Say <SOMETHING> to the skaters in your area.",
                                  ["/"],
                                  {"player":True})

    def run(self, player, server, arguments):
        message = OutputUtility.addHtmlTag(player.getUsername() + ": ", "chatname")

        if (player.isGhost()):
            message += " ECHOS IN THE WIND \"" + arguments + "\""
        else:
            message += arguments

        return CommandResponse(player, server, CommandResponse.SEND_TO_PLAYER, "", { "broadcast": message })
