from command import *
from utility import *
from responses.commandresponse import *

class Move( Command ):
    def __init__( self ):
        super( Move, self ).__init__( "Move",
                                      "Move to <SOMEWHERE>",
                                      [ "GO", "SAUNTER", "EXIT", "ENTER" ],
                                      { 'player': True } )

    def generateMoveMessage( self, relativeLocationData, newLocation ):
        useTowards = relativeLocationData.useTowards
        article = "the"
        if ( newLocation.article ):
            article = newLocation.article
        message = "Moved " + ( "towards the " if useTowards else "" ) +  relativeLocationData.direction
        message += " " + relativeLocationData.preposition + " " + article + " " + newLocation.name

        return message + "\n\n" + str( newLocation )

    def processRelativeLocation( self, player, server, relativeLocationData, preposition, arguments ):
        # do a preposition checks
        prepositionFailureMessage = relativeLocationData.getPrepositionFailureMessage( preposition, arguments )
        if ( prepositionFailureMessage ):
            return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, prepositionFailureMessage, {} )

        player.move( relativeLocationData.shortName )
        currentLocation = player.getCurrentLocation()
        enterResponse = currentLocation.onEnter( player )

        responseMessage = self.generateMoveMessage( relativeLocationData, currentLocation )
        if ( len( enterResponse[0].strip() ) > 0 ):
            responseMessage = enterResponse[0].strip() + "\n" + responseMessage

        commandArguments = enterResponse[1]
        commandArguments[ 'broadcast' ] = player.getUsername() + " has entered " + currentLocation.name

        return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, responseMessage, commandArguments )

    def run( self, player, server, arguments ):
        argumentsSplit = arguments.split()

        # strip off any prepositions
        preposition = None
        if ( len( argumentsSplit ) > 0 and argumentsSplit[0].upper() in GameUtility.PREPOSITIONS ):
            preposition = argumentsSplit[0].upper()
            arguments = ' '.join( argumentsSplit[1:] )

        if ( len( arguments.strip() ) == 0 ):
            return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, "You cannot move towards nothing!", {} )

        direction = arguments.upper()
        locationName = arguments.upper()

        currentLocation = player.getCurrentLocation()
        relativeLocationDataByDirection = currentLocation.getRelativeLocationByDirection( direction )
        relativeLocationDataByName = currentLocation.getRelativeLocationByName( locationName )
        if ( relativeLocationDataByDirection ):
            return self.processRelativeLocation( player, server, relativeLocationDataByDirection, preposition, arguments )
        elif ( relativeLocationDataByName ):
            return self.processRelativeLocation( player, server, relativeLocationDataByName, preposition, arguments )
        else:
            return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, "There is nothing of interest at \"" + arguments + "\"", {} )
