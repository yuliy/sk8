from command import *
from responses.commandresponse import *

class Logout( Command ):
    def __init__( self ):
        super( Logout, self ).__init__( "Logout",
                                        "Logout of the Game.",
                                        [ "QUIT", "DC", "TURBOQUIT" ],
                                        { 'player': True } )

    def run( self, player, server, arguments ):
        return CommandResponse( player, server, CommandResponse.LOGOUT, "", {} )
        