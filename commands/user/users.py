from command import *
from responses.commandresponse import *

class Users( Command ):
    def __init__( self ):
        super( Users, self ).__init__( "Users",
                                       "Show the users in your current location.",
                                       [ "PLAYERS" ],
                                       { 'player': True } )

    def run( self, player, server, arguments ):
        return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, server.getPlayersInLocation( player.getCurrentLocation() ), {} )
