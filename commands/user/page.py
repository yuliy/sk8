from command import *
from utility import *
from responses.commandresponse import *

class Page( Command ):
    def __init__( self ):
        super( Page, self ).__init__( "Page",
                                      "Page <USERNAME> with <MESSAGE>.",
                                      [],
                                      { 'player': True } )

    def run( self, player, server, arguments ):
        argumentsSplit = arguments.split()
        if ( not len( argumentsSplit ) >= 2 ):
            return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, "You need to include a Username and a Message in your page!", {} )

        username = arguments.split()[0]
        message = ' '.join( arguments.split()[1:] )
        toMessage = "From " + OutputUtility.addColorTag( player.getUsername().lower(), ColorMap.GREEN ) + ": " + message
        toMessage = ScriptingUtility.applyTags( toMessage, player )

        response = server.sendToUsername( username, toMessage, { "override": "Y" } )
        responseMessage = ""
        if ( response ):
            responseMessage = "To " + OutputUtility.addColorTag( username.lower(), ColorMap.GREEN ) + ": " + message
            responseMessage = ScriptingUtility.applyTags( responseMessage, player )
        else:
            responseMessage = "That player " + username.lower() + " does not exist or is not logged in!"

        return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, responseMessage, { "override": "Y" } )
