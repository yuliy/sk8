from command import *
from responses.commandresponse import *

class Dance( Command ):
    def __init__( self ):
        super( Dance, self ).__init__( "Dance",
                                       "At your own risk...",
                                       [ "BREAK" ],
                                       { 'player': True } )

    def run( self, player, server, arguments ):
        return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, "You begin to dance. A Dagger approaches your and reveals taht \"BREAKING IS A MEMORY\".  They disappear...", {} )