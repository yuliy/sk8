from command import *
from responses.commandresponse import *

class Inventory( Command ):
    def __init__( self ):
        super( Inventory, self ).__init__( "Inventory",
                                           "Display the contents of your inventory.",
                                           [ "ITEMS" ],
                                           { 'player': True } )

    def run( self, player, server, arguments ):
        if ( player.isGhost() ):
            return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, "Ghosts have no need for wordly possessions.", {} )

        inventoryString = "INVENTORY\n"

        itemStrings = {}
        maxLength = -1

        for itemName in player.getItems():
            item = player.itemFactory.getItem( itemName.upper() )
            if ( item ):
                itemString = str( item )
                if ( len( itemString ) > maxLength ):
                    maxLength = len( itemString )
                itemStrings[ itemName ] = ( item, itemString )

        for itemName in itemStrings:
            itemCouple = itemStrings[ itemName ]
            item = itemCouple[0]
            itemString = itemCouple[1]

            lengthDifference = maxLength - len( itemString ) + 4
            inventoryString += itemString
            for i in range( 0, lengthDifference ):
                inventoryString += "."

            quantity = player.getItemQuantity(itemName)

            for i in range( 0, 3 ):
                if ( float( quantity ) / float( pow( 10, 3 - i ) ) < 1 ):
                    inventoryString += "."

            inventoryString += str( quantity ) + "\n"

        # strip off last return carriage
        inventoryString = inventoryString.strip()

        return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, inventoryString, {} )
        