from command import *
from responses.commandresponse import *

class Haunt( Command ):
    def __init__( self ):
        super( Haunt, self ).__init__( "Haunt",
                                       "...",
                                       [],
                                       { 'player': True } )

    def run( self, player, server, arguments ):
        if ( player.isGhost() ):
            return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, "You made the area marginally more scary.", {} )
        else:
            return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, "You try hard, but you simply are not that scary in your current form.", {} )