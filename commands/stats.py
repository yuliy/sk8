from command import *
from utility import *
from responses.commandresponse import *

class Stats( Command ):
    def __init__( self ):
        super( Stats, self ).__init__( "Stats",
                                       "Display your Stats.",
                                       [],
                                       { 'player': True } )

    def run( self, player, server, arguments ):
        return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, str( player ), {} )
        