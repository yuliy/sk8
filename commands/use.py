from command import *
from responses.commandresponse import *

class Use( Command ):
    def __init__( self ):
        super( Use, self ).__init__( "Use",
                                     "Use <SOMETHING>",
                                     [],
                                     { 'player': True } )

    def run( self, player, server, arguments ):
        if ( len( arguments.strip() ) == 0 ):
            return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, "You cannot use nothing!", {} )

        entity = player.getWorld().findEntity( player, arguments )
        item = player.itemFactory.getItem( arguments.upper() )

        if ( entity ):
            action = entity.getAction( "USE" )
            if ( action ):
                actionResponse = action.perform( player )
                actionResponse.addArgument( "override", "Y" )
                return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, actionResponse.getMessage(), actionResponse.getArguments() )
            else:
                interactionMessage = entity.getInteractionMessage( "USE" )
                responseMessage = interactionMessage if interactionMessage else "You cannot use that \"" + entity.entityType + "\""
                return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, responseMessage, { "override": "Y" } )
        elif ( item ):
            action = item.getAction( "USE" )
            if ( action ):
                actionResponse = action.perform( player )
                return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, actionResponse.getMessage(), actionResponse.getArguments() )
            else:
                interactionMessage = item.getInteractionMessage( "USE" )
                responseMessage = interactionMessage if interactionMessage else "You cannot use that \"" + item.entityType + "\""
                return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, responseMessage, {} )
        else:
            return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, "There is no \"" + arguments + "\"!", {} )
