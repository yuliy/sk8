from command import *
from responses.commandresponse import *

class Read( Command ):
    def __init__( self ):
        super( Read, self ).__init__( "Read",
                                      "Read <SOMETHING>",
                                      [],
                                      { 'player': True } )

    def run( self, player, server, arguments ):
        if ( len( arguments.strip() ) == 0 ):
            return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, "You cannot read nothing!", {} )

        entity = player.getWorld().findEntity( player, arguments )

        if ( entity ):
            action = entity.getAction( "READ" )
            if ( action ):
                actionResponse = entity.getAction( "READ" ).perform( player )
                actionResponse.addArgument( "override", "Y" )
                return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, actionResponse.getMessage(), actionResponse.getArguments() )
            else:
                interactionMessage = entity.getInteractionMessage( "READ" )
                responseMessage = interactionMessage if interactionMessage else "You cannot read that \"" + entity.entityType +"\""
                return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, responseMessage, { "override": "Y" } )
        else:
            return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, "There is no \"" + arguments + "\"...", {} )
            