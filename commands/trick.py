from command import *
from responses.commandresponse import *

class Trick( Command ):
    def __init__( self ):
        super( Trick, self ).__init__( "Session",
                                       "Start a Session.",
                                       [ "TRICK", "SESS", "SESH" ],
                                       { 'player': True } )

    def run( self, player, server, arguments ):
        if ( len( arguments.strip() ) == 0 ):
            if ( not player.isInSession() ):
                sessionResponse = player.getSessionController().advance( "TRICK", arguments )
                return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, sessionResponse.getMessage(), {} )
            else:
                return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, str( player.getCurrentSession() ), {} )
        else:
            return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, "Session commands takes no arguments.", {} )
        