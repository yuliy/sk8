from command import *
from responses.commandresponse import *

class RP( Command ):
    def __init__( self ):
        super( RP, self ).__init__( "RP",
                                    "Good for the role player's.",
                                    [ "ME", "EMOTE" ],
                                    { 'player': True } )

    def run( self, player, server, arguments ):
    	if ( len( arguments.strip() ) == 0 ):
    		arguments = "considers themself"

        message = player.getUsername() + " " + arguments
        message = OutputUtility.addItalicsTag(message)

        return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, "", { "broadcast": message } )
            