from command import *
from responses.commandresponse import *

class Give( Command ):
    def __init__( self ):
        super( Give, self ).__init__( "Give", "", [], { 'player_admin': True, 'admin': True } )

    def run( self, player, server, arguments ):
        split = arguments.split()
        if ( len( split ) == 1 ):
            itemType = split[0].upper()
            gaveItem = player.giveItem( itemType )

            if ( gaveItem ):
                item = player.itemFactory.getItem( itemType )
                if ( player.isAdmin() ):
                    return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, "YOU GAVE YOURSELF " + str( 1 ) + " " + str( item ), {} )
                else:
                    return CommandResponse( player, server, CommandResponse.ADMIN_RESPONSE, "YOU GOT " + str( 1 ) + " " + str( item ), {} )
            else:
                if ( player.isAdmin() ):
                    return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, "FAILED TO GIVE YOUSELF " + itemType, {} )
                else:
            	   return CommandResponse( player, server, CommandResponse.ADMIN_RESPONSE, "ERROR", {} )
        elif ( len( split ) == 2 ):
            itemType = split[1].upper()
            otherPlayer = server.findPlayer( split[0] ) # will only find other players who are currently logged in

            if ( otherPlayer ):
                gaveItem = otherPlayer.giveItem( itemType )
                if ( gaveItem ):
                    item = player.itemFactory.getItem( itemType )
                    message = "YOU GAVE " + str( 1 ) + " " + str( item ) + " TO " + otherPlayer.getUsername()
                    messagToOtherPlayer = "YOU GOT " + str( 1 ) + " " + str( item ) + " FROM " + player.getUsername()

                    response = server.sendToUsername( otherPlayer.getUsername(), messagToOtherPlayer, { "override": "Y" } )
                    if ( response ):
                        return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, message, {} )
                    else:
                        return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, "THAT PLAYER DOES NOT EXIST", {} )
                else:
                    message = "CANNOT GIVE " + itemType + " TO " + otherPlayer.getUsername()
                    return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, message, {} )
            else:
                return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, "THAT PLAYER DOES NOT EXIST", {} )
        else:
            return CommandResponse( player, server, CommandResponse.ADMIN_RESPONSE, "ERROR", {} )
