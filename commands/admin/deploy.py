import os

from fabric.network import disconnect_all

from command import *
from responses.commandresponse import *
from build.fabfile import *


class Deploy(Command):
    NOT_ADMIN_ERROR_MESSAGE = "Cannot run deploy on a non-admin server"
    USAGE_ERROR_MESSAGE = "Must specify a deployment name and branch name to deploy (i.e. develop dev develop)"
    FRONTEND_EXCEPTION_MESSAGE = "Failed to redeploy frontend app service due to exception..."
    BACKEND_EXCEPTION_MESSAGE = "Failed to redeploy backend app service due to exception..."
    FAILURE_MESSAGE = "Application Failed to Deploy"
    SUCCESS_MESSAGE = "Backend and Frontend Services Deployed Successfully"

    def __init__(self):
        super(Deploy, self).__init__("Deploy", "", [], {'player_admin': True, 'admin': True})

    def _build_success_message(self, deployment, branch):
        """
        Utility function to generate the success message for deploying the application.

        :param deployment:
        :param branch:
        :return:
        """
        message = Deploy.SUCCESS_MESSAGE + " to " + OutputUtility.addColorTag(deployment, ColorMap.GREEN)
        message += " with branch " + OutputUtility.addColorTag(branch, ColorMap.GREEN)
        return message

    def _build_failure_message(self, deployment, branch, exception, target):
        """
        Utility function to generate the failure message for failing to deploy the application.

        :param deployment:
        :param branch:
        :param exception:
        :return:
        """
        message = Deploy.FAILURE_MESSAGE + " to " + OutputUtility.addColorTag(deployment, ColorMap.GREEN)
        message += " with branch " + OutputUtility.addColorTag(branch, ColorMap.GREEN) + "\n"
        if target == "backend":
            message += Deploy.BACKEND_EXCEPTION_MESSAGE
        elif target == "frontend":
            message += Deploy.FRONTEND_EXCEPTION_MESSAGE
        message += str(exception)

        return message

    def run(self, player, server, arguments):
        """
        Overriding the abstract version of the Command.run method so that we can do the work of redeploying the
        application to the backend and frontend servers.

        Sample in-game usage: deploy DEPLOYMENT_NAME BRANCH_NAME

        :param player:
        :param server:
        :param arguments:
        :return:
        """
        # ensure that the server we are connected to is in admin mode
        if not server.get_is_admin_mode():
            return CommandResponse(player, server, CommandResponse.SEND_TO_PLAYER, Deploy.NOT_ADMIN_ERROR_MESSAGE)

        # retrieve arguments and ensure that both a deployment and a branch name are specified
        arguments = arguments.split()
        if not len(arguments) == 2:
            return CommandResponse(player, server, CommandResponse.SEND_TO_PLAYER, Deploy.USAGE_ERROR_MESSAGE)

        deployment = arguments[0].lower()
        branch = arguments[1]

        # setup the host_string setting so that we can use our Fabric tasks and try to deploy the backend app service
        deployment_config = get_deployment_config(deployment)
        with settings(host_string=deployment_config["backend"]["host"], abort_exception=Exception):
            try:
                deploy(branch, deployment, TARGET_BACKEND, deployment_config)
            except Exception, e:
                server.printServerMessage(Deploy.BACKEND_EXCEPTION_MESSAGE + str(e))
                return CommandResponse(player,
                                       server,
                                       CommandResponse.SEND_TO_PLAYER,
                                       self._build_failure_message(deployment, branch, e, "backend"))
            finally:
                disconnect_all()  # regardless of the success of the deployment we must close all connections

        # setup the host_string setting so that we can use our Fabric tasks and try to deploy the frontend app service
        with settings(host_string=deployment_config["frontend"]["host"], abort_exception=Exception):
            try:
                deploy(branch, deployment, TARGET_FRONTEND, deployment_config)
            except Exception, e:
                server.printServerMessage(Deploy.FRONTEND_EXCEPTION_MESSAGE + str(e))
                return CommandResponse(player,
                                       server,
                                       CommandResponse.SEND_TO_PLAYER,
                                       self._build_failure_message(deployment, branch, e, "frontend"))
            finally:
                disconnect_all()  # regardless of the success of the deployment we must close all connections

        # setup the success response
        message = self._build_success_message(deployment, branch)
        server.printServerMessage(Deploy.SUCCESS_MESSAGE)
        return CommandResponse(player, server, CommandResponse.SEND_TO_PLAYER, message)