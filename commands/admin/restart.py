import os

from fabric.network import disconnect_all

from command import *
from responses.commandresponse import *
from build.fabfile import *


class Restart(Command):
    NOT_ADMIN_ERROR_MESSAGE = "Cannot run restart on a non-admin server"
    USAGE_ERROR_MESSAGE = "Must specify a deployment name to restart (i.e. restart dev)"
    FRONTEND_EXCEPTION_MESSAGE = "Failed to restart frontend app service due to exception..."
    BACKEND_EXCEPTION_MESSAGE = "Failed to restart backend app service due to exception..."
    FAILURE_MESSAGE = "Application Failed to Restart"
    SUCCESS_MESSAGE = "Backend and Frontend Services Restart Successfully"

    def __init__(self):
        super(Restart, self).__init__("Restart", "", [], {'player_admin': True, 'admin': True})

    def _build_success_message(self, deployment):
        """
        Utility function to generate the success message for restarting the application.

        :param deployment:
        :return:
        """
        message = Restart.SUCCESS_MESSAGE + " on deployment " + OutputUtility.addColorTag(deployment, ColorMap.GREEN)
        return message

    def _build_failure_message(self, deployment, exception, target):
        """
        Utility function to generate the failure message for failing to restart the application.

        :param deployment:
        :param exception:
        :param target:
        :return:
        """
        message = Restart.FAILURE_MESSAGE + " on deployment "
        message += OutputUtility.addColorTag(deployment, ColorMap.GREEN) + "\n"
        if target == "backend":
            message += Restart.BACKEND_EXCEPTION_MESSAGE
        elif target == "frontend":
            message += Restart.FRONTEND_EXCEPTION_MESSAGE
        message += str(exception)

        return message

    def run(self, player, server, arguments):
        """
        Overriding the abstract version of the Command.run method so that we can do the work of starting the application
        on a certain deployment from an admin instance of the game server.

        Sample in-game usage: restart DEPLOYMENT_NAME

        :param player:
        :param server:
        :param arguments:
        :return:
        """
        # ensure that the server we are connected to is in admin mode
        if not server.get_is_admin_mode():
            return CommandResponse(player, server, CommandResponse.SEND_TO_PLAYER, Restart.NOT_ADMIN_ERROR_MESSAGE)

        # retrieve deployment name from the arguments
        arguments = arguments.split()
        if not len(arguments) == 1:
            return CommandResponse(player, server, CommandResponse.SEND_TO_PLAYER, Restart.USAGE_ERROR_MESSAGE)

        deployment = arguments[0].lower()

        # setup the host_string setting so that we can use our Fabric tasks and try to restart the backend service
        deployment_config = get_deployment_config(deployment)
        with settings(host_string=deployment_config["backend"]["host"], abort_exception=Exception):
            try:
                restart(deployment, TARGET_BACKEND, deployment_config)
            except Exception, e:
                server.printServerMessage(Restart.BACKEND_EXCEPTION_MESSAGE + str(e))
                return CommandResponse(player,
                                       server,
                                       CommandResponse.SEND_TO_PLAYER,
                                       self._build_failure_message(deployment, e, "backend"))
            finally:
                disconnect_all()  # regardless of the success of the restart we must close all connections

        # setup the host_string setting so that we can use our Fabric tasks and try to restart the frontend service
        with settings(host_string=deployment_config["frontend"]["host"], abort_exception=Exception):
            try:
                restart(deployment, TARGET_FRONTEND, deployment_config)
                pass
            except Exception, e:
                server.printServerMessage(Restart.FRONTEND_EXCEPTION_MESSAGE + str(e))
                return CommandResponse(player,
                                       server,
                                       CommandResponse.SEND_TO_PLAYER,
                                       self._build_failure_message(deployment, e, "frontend"))
            finally:
                disconnect_all()  # regardless of the success of the restart we must close all connections

        # setup the success response
        message = self._build_success_message(deployment)
        server.printServerMessage(Restart.SUCCESS_MESSAGE)
        return CommandResponse(player, server, CommandResponse.SEND_TO_PLAYER, message)