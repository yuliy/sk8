from command import *
from responses.commandresponse import *

class PostTweet( Command ):
    def __init__( self ):
        super( PostTweet, self ).__init__( "PostTweet", "", [], { 'admin': True } )

    def run( self, player, server, arguments ):
        arguments = arguments.replace( Command.USERNAME_CODE, player.getUsername() )
        status = server.getTwitterConnection().PostUpdate( arguments )
        return CommandResponse( player, server, CommandResponse.ADMIN_RESPONSE, "", {} )
