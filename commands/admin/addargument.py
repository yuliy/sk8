from command import *
from responses.commandresponse import *

class AddArgument( Command ):
    def __init__( self ):
        super( AddArgument, self ).__init__( "AddArgument",
                                             "",
                                             [],
                                             { 'admin': True } )

    def run( self, player, server, arguments ):
    	splitArguments = arguments.split()
    	if ( len( splitArguments ) == 2 ):
        	return CommandResponse( player, server, CommandResponse.ADMIN_RESPONSE, "", { splitArguments[0]: splitArguments[1] } )
        else:
        	return CommandResponse( player, server, CommandResponse.ADMIN_RESPONSE, "", {} )
