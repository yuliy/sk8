from command import *
from responses.commandresponse import *

class Display( Command ):
    def __init__( self ):
        super( Display, self ).__init__( "Display", "", [], { 'admin': True } )

    def run( self, player, server, arguments ):
        arguments = arguments.replace( CommandResponse.USERNAME_CODE, player.getUsername() )
        return CommandResponse( player, server, CommandResponse.ADMIN_RESPONSE, "", { "broadcast": arguments } )
        