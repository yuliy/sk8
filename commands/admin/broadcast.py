from command import *
from responses.commandresponse import *

class Broadcast( Command ):
    def __init__( self ):
        super(Broadcast, self).__init__("Broadcast", "Broadcast a message to everyone", [], {'player_admin': True, 'admin': True})

    def run(self, player, server, arguments):
        message = "Broadcast from " + OutputUtility.addHtmlTag(player.getUsername() + ": ", "chatname")
        message += arguments

        if (player.isAdmin()):
            return CommandResponse(player, server, CommandResponse.SEND_TO_ALL, message, {})

        return None
