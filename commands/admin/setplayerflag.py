from command import *
from responses.commandresponse import *

class SetPlayerFlag( Command ):
    def __init__( self ):
        super( SetPlayerFlag, self ).__init__( "SetPlayerFlag", "", [], { 'admin': True } )

    def run( self, player, server, arguments ):
        argumentsSplit = arguments.split()
        if ( len( argumentsSplit ) == 2 ):
            flag = argumentsSplit[0].upper()
            value = argumentsSplit[1]

            player.setPlayerFlag( flag, value )
            return CommandResponse( player, server, CommandResponse.ADMIN_RESPONSE, "", {} )

        return CommandResponse( player, server, CommandResponse.ADMIN_RESPONSE, "ERROR", {} )
        