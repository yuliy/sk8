from command import *
from responses.commandresponse import *

class Teleport( Command ):
    def __init__( self ):
        super( Teleport, self ).__init__( "Teleport", "", [], { 'admin': True } )

    def run( self, player, server, arguments ):
        updatedLocationShortName = arguments
        player.move( updatedLocationShortName )
        currentLocation = player.getCurrentLocation()
        enterResponse = currentLocation.onEnter( player )

        responseMessage = str( player.getCurrentLocation() )
        if ( len( enterResponse[0].strip() ) > 0 ):
        	responseMessage = enterResponse[0].strip() + "\n" + responseMessage

        commandArguments = enterResponse[1]
        commandArguments[ 'broadcast' ] = player.getUsername() + " has entered " + currentLocation.name

        return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, responseMessage, commandArguments )
        