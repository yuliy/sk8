from command import *
from responses.commandresponse import *

class Restore( Command ):
    def __init__( self ):
        super( Restore, self ).__init__( "Restore", "", [], { 'admin': True } )

    def run( self, player, server, arguments ):
        restoreMessage = ""
        if ( not arguments ): # we got no amount to restore so we should just restore everything
            player.restoreHealth()
            health = player.getHealth()
            restoreMessage = "YOUR HEALTH IS FULLY RESTORED!  CURRENT HEALTH: " + str( health[ "current" ] ) + "/" + str( health[ "max" ] )
        else:
            amount = int( arguments )
            player.restoreHealth( amount )
            health = player.getHealth()
            restoreMessage = "YOU RESTORED " + str( amount ) + " HEALTH!  CURRENT HEALTH: " + str( health[ "current" ] ) + "/" + str( health[ "max" ] )

        return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, restoreMessage, {} )
