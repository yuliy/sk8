from command import *
from responses.commandresponse import *

class SetPlayerMode( Command ):
    def __init__( self ):
        super( SetPlayerMode, self ).__init__( "SetPlayerMode", "", [], { 'admin': True } )

    def run( self, player, server, arguments ):
        mode = arguments.lower()
        modeSet = player.setPlayerMode( mode )
        return CommandResponse( player, server, CommandResponse.ADMIN_RESPONSE, "" if modeSet else "ERROR", {} )
