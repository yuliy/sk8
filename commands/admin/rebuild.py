from command import *
from responses.commandresponse import *

class Rebuild( Command ):
    def __init__( self ):
        super( Rebuild, self ).__init__( "Rebuild", "", [], { 'player_admin': True, 'admin': True } )

    def run( self, player, server, arguments ):
        if ( player.isAdmin() ):
            server.buildWorld()

        return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, "World rebuilt" )