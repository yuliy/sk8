from command import *
from responses.commandresponse import *

class UnlockTrick( Command ):
    def __init__( self ):
        super( UnlockTrick, self ).__init__( "UnlockTrick", "", [], { 'admin': True } )

    def run( self, player, server, arguments ):
        unlockResult = player.unlockTrick( arguments.upper() )

        # if we actually unlocked a trick through this command
        if ( unlockResult ):
        	return CommandResponse( player, server, CommandResponse.ADMIN_RESPONSE, "You unlocked the trick: " + arguments.upper() + "!" )
        
        return None
