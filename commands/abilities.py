from command import *
from responses.commandresponse import *

class Abilities( Command ):
    def __init__( self ):
        super( Abilities, self ).__init__( "Abilities",
                                           "Tricks - Displays Available Tricks",
                                           [ "TRICKS" ],
                                           { 'player': True } )

    def run( self, player, server, arguments ):
        availableTricks = player.getSessionController().getOverworldTricks()

        if ( len( arguments.strip() ) == 0 ):
            abilities = ""

            if ( len( availableTricks ) > 0 ):
                abilities += "\n\nAvailable Tricks:\n"
                for trickName in availableTricks:
                    trick = availableTricks[ trickName ]
                    abilities += trickName + " - " + trick.help + "\n"
            else:
                abilities = "You do not have any available tricks..."

            return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, abilities.strip(), { "override": "Y" } )
        else:
            if commandName in availableTricks:
                return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, availableTricks[ commandName ].help, { "override": "Y" } )
            else:
                return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, "That is not a trick I know about...", {} )
