from command import *
from player import *
from responses.commandresponse import *

class Create(Command):
    CREATE_ALREADY_CONNECTED = "Already Logged In"
    CREATE_ARGUMENT_FAILURE = "Create failed...please provide format CREATE username password"
    CREATE_INVALID_USERNAME = "Server: Please enter a valid username"
    CREATE_USER_EXISTS = "Create failed...username already exists"

    def __init__(self):
        super(Create, self).__init__("Create",
                                     "Create a new user",
                                     [],
                                     {'player': True})

    def generateLoginMessage(self, isNew, player, server):
        locationMessage = player.processCommand(server, "EXAMINE", "EXAMINE", "").getMessage()
        loginSuccess = ("Successfully logged in as" + 
                        (" new" if isNew else "") + " user " + 
                        player.getUsername() + 
                        ".\n\nType \"HELP\" for help.\n\n" + locationMessage)

        return loginSuccess

    def run(self, player, server, arguments):
        # check if the player is not logged in
        if (player.isDisconnected()):
            argumentsList = arguments.split()
            if (len(argumentsList) != 2):
                return CommandResponse(player,
                                       server,
                                       CommandResponse.SEND_TO_PLAYER,
                                       Create.CREATE_ARGUMENT_FAILURE,
                                       {})

            # verify username input
            username = argumentsList[0]
            if (len(username.strip()) == 0 or ("\\" in username)):
                return CommandResponse(player, 
                                       server, 
                                       CommandResponse.SEND_TO_PLAYER,
                                       Create.CREATE_INVALID_USERNAME,
                                       {})
            elif (server.usernameInUse(username)):
                return CommandResponse(player, 
                                       server, 
                                       CommandResponse.SEND_TO_PLAYER, 
                                       "Server: Username " + username + " is already in use.",
                                       {})

            # verify password input
            password = argumentsList[1]

            # check if the player with this username already exists
            if (not server.userExists(username)):
                # create a new user based on the username/password
                server.printServerMessage("Creating new user " + username)
                player.setupUser(username, password)
                return CommandResponse(player,
                                       server,
                                       CommandResponse.SEND_TO_PLAYER,
                                       self.generateLoginMessage(True, player, server),
                                       {"override": "Y"})
            else:
                return CommandResponse(player,
                                       server,
                                       CommandResponse.SEND_TO_PLAYER,
                                       Create.CREATE_USER_EXISTS,
                                       {})
        else:
            return CommandResponse(player,
                                   server,
                                   CommandResponse.SEND_TO_PLAYER,
                                   Create.CREATE_ALREADY_CONNECTED,
                                   {})
        