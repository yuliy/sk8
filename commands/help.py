from command import *
from responses.commandresponse import *

class Help( Command ):
    def __init__( self ):
        super( Help, self ).__init__( "Help", 
                                      "Display the Game commands.", 
                                      [ "?", "H", "COMMANDS", "HALP" , "HUH?", "WHAT?" ], 
                                      { 'player': True } )

    def addAvailableCommands( self, player, commandList, helpString ):
        for command in commandList:
            helpString += player.getCommand( command ).helpMessage + "\n"

        return helpString.strip()

    def run( self, player, server, arguments ):
        if ( len( arguments.strip() ) == 0 ):
            helpString = ""
            commands = player.getCommands()
            helpString = self.addAvailableCommands( player, commands, helpString )

            if ( 'SESSION' in commands ):
                availableTricks = player.getSessionController().getOverworldTricks()
                if ( len( availableTricks ) > 0 ):
                    helpString += "\n\nAVAILABLE TRICKS:\n"
                    for trickName in availableTricks:
                        trick = availableTricks[ trickName ]
                        helpString += trickName + " - " + trick.help + "\n"

            return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, helpString.strip(), {} )
        else:
            commandName = arguments
            commands = player.getCommands()

            if ( commandName in commands ):
                return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, player.getCommand( commandName ).helpMessage, {} )
            else:
                if ( 'SESSION' in commands ):
                    availableTricks = player.getSessionController().getOverworldTricks()
                    if ( commandName in availableTricks ):
                        return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, availableTricks[ commandName ].help, {} )

            return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER,  "That is not a valid command.  Type \"HELP\" to display all commands.", {} )
