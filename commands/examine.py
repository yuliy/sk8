from command import *
from responses.commandresponse import *

class Examine( Command ):
    def __init__( self ):
        super( Examine, self ).__init__( "Examine",
                                         "Examine <SOMETHING>",
                                         [ "CHECK", "X" , "LOOK" ],
                                         { 'player': True } )

    def run( self, player, server, arguments ):
        if ( len( arguments.strip() ) == 0 ): # if we pass no arguments to this command then just print out what is in the player location
            location = str( player.getCurrentLocation() )
            locationPlayers = server.getPlayersInLocation( player.getCurrentLocation() )
            message = location + "\n" + locationPlayers
            return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, message, { "override": "Y" } )
        
        if ( arguments.split()[0].lower() == "at" ): # ignore the word "at" in command signature
            arguments = ' '.join( arguments.split()[1:] )
        
        # check if this is a world entity
        entityType = arguments
        entity = player.getWorld().findEntity( player, entityType )
        if ( entity ):
            return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, entity.getDescription(), { "override": "Y" } )

        # check if this is an item
        item = player.itemFactory.getItem( entityType )
        if ( item ):
            return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, item.getDescription(), { "override": "Y" } )

        # otherwise it doesn't exist
        return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, "There is no \"" + entityType + "\"...", {} )
