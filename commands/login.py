from command import *
from player import *
from responses.commandresponse import *

class Login(Command):
    LOGIN_ALREADY_CONNECTED = "Already Logged In"
    LOGIN_ARGUMENT_FAILURE = "Login failed...please provide format LOGIN username password"
    LOGIN_INVALID_USERNAME = "Server: Please enter a valid username"

    def __init__(self):
        super(Login, self).__init__("Login",
                                    "Login to the Game",
                                    [],
                                    {'player': True})

    def generateLoginMessage(self, isNew, player, server):
        locationMessage = player.processCommand(server, "EXAMINE", "EXAMINE", "").getMessage()
        loginSuccess = ("Successfully logged in as" + 
                        (" new" if isNew else "") + " user " + 
                        player.getUsername() + 
                        ".\n\nType \"HELP\" for help.\n\n" + locationMessage)

        return loginSuccess

    def run(self, player, server, arguments):
        # check if the player is not logged in
        if (player.isDisconnected()):
            argumentsList = arguments.split()
            if (len(argumentsList) != 2):
                return CommandResponse(player,
                                       server,
                                       CommandResponse.SEND_TO_PLAYER,
                                       Login.LOGIN_ARGUMENT_FAILURE,
                                       {})

            # verify username input
            username = argumentsList[0]
            if (len(username.strip()) == 0 or ("\\" in username)):
                return CommandResponse(player, 
                                       server, 
                                       CommandResponse.SEND_TO_PLAYER, 
                                       Login.LOGIN_INVALID_USERNAME,
                                       {})
            elif (server.usernameInUse(username)):
                return CommandResponse(player, 
                                       server, 
                                       CommandResponse.SEND_TO_PLAYER, 
                                       "Server: Username " + username + " is already in use.",
                                       {})

            # verify password input
            password = argumentsList[1]

            # check if the player with this username already exists
            if (server.userExists(username)):
                server.printServerMessage("Found save file for user " + username)
                oldMode = player.load(username)

                # password authentication
                if (player.authenticate(password)):
                    # authentication succeeded, keep the loaded player and notify the client
                    server.printServerMessage("Sucessfully loaded user " + username)

                    player.client.username = username
                    player.completeLogin(oldMode)
                    return CommandResponse(player,
                                           server,
                                           CommandResponse.SEND_TO_PLAYER,
                                           self.generateLoginMessage(False, player, server),
                                           {"override": "Y"})
                else:
                    # authentication, reset the player and notify the client
                    player.setupData()
                    return CommandResponse(player, 
                                           server, 
                                           CommandResponse.SEND_TO_PLAYER, 
                                           "Incorrect password for user " + username + ".  Please try to login again.",
                                           {})
            else:
                # user does not exist, notify the client
                return CommandResponse(player, 
                                       server, 
                                       CommandResponse.SEND_TO_PLAYER, 
                                       "User " + username + " does not exist.  Please try to login again.",
                                       {})
        else:
            return CommandResponse(player,
                                   server,
                                   CommandResponse.SEND_TO_PLAYER,
                                   Login.LOGIN_ALREADY_CONNECTED,
                                   {})
        