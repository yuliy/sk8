from command import *
from responses.commandresponse import *

class Tag( Command ):
    def __init__( self ):
        super( Tag, self ).__init__( "Tag",
                                     "Tag <SOMETHING> with <MESSAGE>",
                                     [],
                                     { 'player': True } )

    def run( self, player, server, arguments ):
        argumentsSplit = arguments.split()
        if ( not len( argumentsSplit ) >= 2 ):
            return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, "You need to include an Entity and a Message in your tag!", {} )

        entityType = arguments.split()[0]
        entity = player.getWorld().findEntity( player, entityType )

        if ( not entity ):
            return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, "There is no \"" + entityType + "\" to tag!", {} )

        message = ' '.join( arguments.split()[1:] )
        entity.tag = message

        return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, "You tagged the " + entityType + " with the message \"" + message + "\"", {} )
        