from command import *
from responses.commandresponse import *

class End( Command ):
    def __init__( self ):
        super( End, self ).__init__( "End",
                                     "End a Trick Session",
                                     [ "LAND" ],
                                     { 'player': True } )

    def run( self, player, server, arguments ):
        return CommandResponse( player, server, CommandResponse.SEND_TO_PLAYER, player.getSessionController().endSession(), {} )
        