import json

from xml.dom.minidom import *

from player import *
from session import *
from responses.sessionresponse import *
from utility import *

class SessionController( object ):
    INVALID_SESSION_COMMAND = "CAN'T DO THAT..."
    INVALID_TRICK = "THAT'S NOT SOMETHING I CAN DO..."

    def __init__(self, player):
        super(SessionController, self).__init__()

        self.player = player
        self.current = None

        # Import Tricks Data
        self.tricks = {}
        tricksData = json.load(open("_data/player/tricks.json"))
        for trickName in tricksData:
            trick = Trick(trickName, tricksData[trickName])
            self.tricks[trick.name] = trick

    def getTricks(self):
        return self.tricks

    def getCurrentSession(self):
        return self.current

    def getOverworldTricks(self): # return all ground tricks and tricks that can be done post ollie
        response = None
        if (not self.player.isInSession()):
            self.startSession(True) # start a simulation session
            groundTricks = self.current.getAvailableTricks()
            self.advance("OLLIE", "", True) # this is a simulation so we don't need to check the response
            airTricks = self.current.getAvailableTricks()
            for trick in airTricks:
                if (not trick in groundTricks):
                    groundTricks[trick] = airTricks[trick]

            self.endSession()

            response = groundTricks
        else:
            response = self.current.getAvailableTricks()

        return response

    def startSession(self, simulation=False):
        self.player.enterSession()

        self.current = Session(self.player, simulation)
        self.current.findAvailableTricks(self.tricks, self.player)
        self.current.findUseableEntities(self.player)
        return Session.SESSION_STARTED + "\n\n" + str(self.current)

    def resetSession(self):
        self.current = Session(self.player)
        self.current.findAvailableTricks(self.tricks, self.player)

    def endSession(self):
        self.player.exitSession()
        return Session.SESSION_ENDED

    def advance( self, command, arguments, single = False ):
        if ( command == "TRICK" and not self.player.isInSession() ): # we just started a session so indicate it
            return SessionResponse( SessionResponse.SESSION_RESPONSE_CONTINUE, self.startSession() )
        elif ( command in self.current.availableTricks ): # we are allowed to do this trick
            # process the trick
            response = self.current.processCommand( self.player, command, arguments )
            responseCode = response.getCode()
            responseMessage = response.getMessage()
            responseArguments = response.getArguments()

            # refresh current session
            self.current.findAvailableTricks( self.tricks, self.player )
            self.current.findUseableEntities( self.player )

            # based on the response object we get determine anything we need to append to the response message
            if ( responseCode == SessionResponse.SESSION_RESPONSE_FAILURE ):
                endResponse = self.endSession()
                if ( not single ):
                    responseMessage += "\nYOU FAILED A TRICK..." + endResponse
            elif ( responseCode == SessionResponse.SESSION_RESPONSE_CONTINUE ):
                if ( not single ): # if this is not a oneshot check if we can advance
                    canAdvanceResponse = self.current.canAdvance()
                    if ( not canAdvanceResponse[0] ): # there are no more tricks that we could do so we must resolve the session
                        responseMessage += "\n" + canAdvanceResponse[1] + self.endSession()
                    elif ( not single ):
                        responseMessage += "\n" + str( self.current )
            elif ( responseCode == SessionResponse.SESSION_RESPONSE_STALL ):
                if ( not single ):
                    responseMessage += ""
            
            response.setMessage( responseMessage )
            response.addArgument( "override", "Y" )
            
            return response
        elif ( command in self.tricks ): # trick isn't available but it does exist
            return SessionResponse( SessionResponse.SESSION_RESPONSE_STALL, SessionController.INVALID_TRICK )
        else: # command entered isn't recognized
            return SessionResponse( SessionResponse.SESSION_RESPONSE_STALL, SessionController.INVALID_SESSION_COMMAND )

    def oneshot( self, command, arguments ):
        overworldTricks = self.getOverworldTricks()
        self.startSession()
        if ( command in self.current.availableTricks ): # if this trick is immediately doable, then execute it
            response = self.advance( command, arguments, True )
            self.endSession()

            return response
        elif ( command in overworldTricks ): # this trick is something we can do in the overworld (one trick off from an ollie)
            trick = self.tricks[ command ]
            if ( len( arguments.strip() ) > 0 ): # check if we have a trick argument
                # preprocess out the preposition from the command
                prepositionResponse = GameUtility.separatePreposition( arguments )
                preposition = prepositionResponse[0]
                arguments = prepositionResponse[1]

                entity = self.player.getWorld().findEntity( self.player, arguments )
                relativeLocationDataByName = self.player.getCurrentLocation().getRelativeLocationByName( arguments )

                if ( entity ): # check to make sure the argument is a valid entity
                    return self.executeCompoundTrick( command, arguments, trick, entity, preposition, entity.hasTrickDifficulty( trick.trickType ) )
                elif ( relativeLocationDataByName ):
                    return self.executeCompoundTrick( command, arguments, trick, entity, preposition, relativeLocationDataByName.hasTrickType( trick.trickType ) )
                else:
                    # make sure to end the session that we started
                    self.endSession()
                    return  SessionResponse( SessionResponse.SESSION_RESPONSE_FAILURE, self.current.generateTrickResponse( trick, None, "DISALLOWED", None, arguments ) )
            else: 
                # if there's no trick argument then we can just try the trick
                return self.executeCompoundTrick( command, arguments, trick, None, "", True )
        else:
            # make sure to end the session that we started
            self.endSession()
            return SessionResponse( SessionResponse.SESSION_RESPONSE_FAILURE, SessionController.INVALID_TRICK )

    def executeCompoundTrick( self, command, arguments, trick, entity, preposition, trickCheck ):
        ollie = self.tricks[ "OLLIE" ]

        # do a preposition check
        if ( entity ):
            prepositionFailureMessage = entity.getPrepositionFailureMessage( preposition, trick.name )
            if ( prepositionFailureMessage ):
                return SessionResponse( SessionResponse.SESSION_RESPONSE_FAILURE, prepositionFailureMessage )
        
        if ( trickCheck ):
            ollieResponse = self.advance( "OLLIE", "", True ) # start it up with an ollie

            if ( not self.player.isInSession() ): # if the ollie failed
                self.endSession()
                responseMessage = self.current.generateOneshotResponse( ollie, trick, ollieResponse, None, entity )
                ollieResponse.setMessage( responseMessage )
                return ollieResponse

            trickResponse = self.advance( command, arguments, True )
            self.endSession()

            # add all the arguments from the ollie into the trick response
            trickResponse.combineArguments( ollieResponse )

            if ( trickResponse.getCode() == SessionResponse.SESSION_RESPONSE_CONTINUE ): # if the trick worked
                responseMessage = self.current.generateOneshotResponse( ollie, trick, ollieResponse, trickResponse, entity )
                trickResponse.setMessage( responseMessage )

                return trickResponse
            else: # otherwise return the error we got from the trick
                return SessionResponse( SessionResponse.SESSION_RESPONSE_FAILURE, trickResponse.getMessage() )
        else:
            self.endSession()
            return SessionResponse( SessionResponse.SESSION_RESPONSE_FAILURE, self.current.generateTrickResponse( trick, entity, "DISALLOWED" ) )
