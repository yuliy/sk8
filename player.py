import random
import json

from itemfactory import *
from sessioncontroller import *
from utility import *

from responses.commandresponse import *

from commands.abilities import *
from commands.create import *
from commands.dance import *
from commands.examine import *
from commands.haunt import *
from commands.help import *
from commands.inventory import *
from commands.login import *
from commands.logout import *
from commands.move import *
from commands.read import *
from commands.say import *
from commands.stats import *
from commands.tag import *
from commands.talk import *
from commands.trick import *
from commands.use import *
from commands.rp import *

from commands.user.page import *
from commands.user.users import *

from commands.session.end import *

from commands.admin.addargument import *
from commands.admin.broadcast import *
from commands.admin.deploy import *
from commands.admin.display import *
from commands.admin.give import *
from commands.admin.halt import *
from commands.admin.posttweet import *
from commands.admin.rebuild import *
from commands.admin.restart import *
from commands.admin.restore import *
from commands.admin.unlocktrick import *
from commands.admin.setplayerflag import *
from commands.admin.setplayermode import *
from commands.admin.teleport import *


class Player:
    PLAYER_DATA_USERNAME = "username"
    PLAYER_DATA_PASSWORD = "password"
    PLAYER_DATA_ADMIN = "admin"
    PLAYER_DATA_MODE = "mode"
    PLAYER_DATA_LOCATION = "location"
    PLAYER_DATA_HEALTH = "health"
    PLAYER_DATA_HEALTH_CURRENT = "current"
    PLAYER_DATA_HEALTH_MAX = "max"
    PLAYER_DATA_STATS = "stats"
    PLAYER_DATA_ITEMS = "items"
    PLAYER_DATA_TRICKS = "tricks"
    PLAYER_DATA_FLAGS = "flags"

    PLAYER_MODE_DISCONNECTED = "disconnected"
    PLAYER_MODE_SKATER = "skater"
    PLAYER_MODE_SESSION = "session"
    PLAYER_MODE_GHOST = "ghost"
    PLAYER_MODE_BABY = "baby"
    PLAYER_MODE_SKELETON = "skelly"

    PLAYER_DEFAULT_ERROR = "Cannot do that!"

    def __init__(self, client):
        # associate with our client instance
        self.client = client

        # associate with our server instance
        self.server = self.client.server

        # setup item controller
        self.itemFactory = ItemFactory()

        # setup session controller
        self.sessionController = SessionController(self)

        # setup player modes
        self.player_modes = [Player.PLAYER_MODE_DISCONNECTED,
                             Player.PLAYER_MODE_SKATER, 
                             Player.PLAYER_MODE_SESSION, 
                             Player.PLAYER_MODE_GHOST, 
                             Player.PLAYER_MODE_BABY, 
                             Player.PLAYER_MODE_SKELETON]

        # setup commands
        self.commands = {}
        self.adminCommands = {}
        self.playerAdminCommands = {}
        self.command_equivalents = {}
        self.base_commands = []
        self.mode_commands = {}
        self.mode_errors = {}
        self.lastCommand = None  # a tuple containing the ( command, arguments ) of that last player input

        self.setupCommands()

        # setup player data
        self.stat_types = [StatUtility.AIR_STAT, StatUtility.GRIND_STAT]
        self.data = {}
        self.setupData()

    def __str__(self):
        if self.isGhost():
            return GameUtility.GHOST_ERROR

        health = self.getHealth()
        nameString = self.getUsername()
        if self.isAdmin():
            nameString += " (admin)"
            
        playerString = "NAME: " + nameString + "\n"
        playerString += "HEALTH: " + str(health[Player.PLAYER_DATA_HEALTH_CURRENT]) + "/"
        playerString += str(health[Player.PLAYER_DATA_HEALTH_MAX]) + "\n"
        playerString += "CURRENT LOCATION: " + self.getCurrentLocation().name + "\n\n"

        statStrings = {}
        maxLength = -1
        for stat in self.stat_types:
            statValue = self.getStat( stat )
            statString = ""
            if ( statValue ):
                statString += stat + ": " + str( statValue )
                statString += " ( " + StatUtility.convertStatValue( statValue ) + " )"

                statStrings[ stat ] = statString
                if ( maxLength == -1 ):
                    maxLength = len( statString )
                else:
                    if ( len( statString ) > maxLength ):
                        maxLength = len( statString )

        for stat in statStrings:
            statString = statStrings[ stat ]

            statCounterValue = self.getStatCounterValue( stat )
            statCounterNextValue = self.getStatCounterNextValue( stat )

            if ( statCounterValue >= 0 and statCounterNextValue >= 0 ):
                progress = float( statCounterValue ) / float( statCounterNextValue )
                progress = int( progress*10 )

                lengthDifference = maxLength - len( statString )

                playerString += statString
                playerString = OutputUtility.appendPadding( playerString, lengthDifference )
                playerString += "\tPROGRESS: |"
                playerString = OutputUtility.appendPadding( playerString, progress, "=" )
                playerString += ">"
                playerString = OutputUtility.appendPadding( playerString, 10-(progress+1) )
                playerString += "|\n"

        return playerString.strip()

    def setupCommand( self, command, isBase, modes ):
        equivalents = command.getEquivalents()
        flags = command.getFlags()

        isPlayerCommand = ( flags['player'] if ( 'player' in flags ) else False )
        isPlayerAdminCommand = ( flags['player_admin'] if ( 'player_admin' in flags ) else False )
        isAdminCommand = ( flags['admin'] if ( 'admin' in flags ) else False )

        if ( isPlayerCommand ):
            self.commands[ command.getName() ] = command

        if ( isPlayerAdminCommand ):
            self.playerAdminCommands[ command.getName() ] = command

        if ( isAdminCommand ):
            self.adminCommands[ command.getName() ] = command

        self.command_equivalents[ command.getName() ] = equivalents

        if ( isBase ):
            self.base_commands.append( command.getName() )

        for mode in modes:
            if not mode in self.mode_commands:
                self.mode_commands[ mode ] = []

            self.mode_commands[ mode ].append( command.getName() )

    def setupCommands( self ):
        # setup base commands
        self.setupCommand(Create(), True, [])
        self.setupCommand(Help(), True, [])
        self.setupCommand(Say(), True, [])
        self.setupCommand(Stats(), True, [])
        self.setupCommand(Inventory(), True, [])
        self.setupCommand(Examine(), True, [])
        self.setupCommand(Move(), True, [])
        self.setupCommand(Dance(), True, [])
        self.setupCommand(Haunt(), True, [])
        self.setupCommand(Login(), True, [])
        self.setupCommand(Logout(), True, [])
        self.setupCommand(Users(), True, [])
        self.setupCommand(Page(), True, [])
        self.setupCommand(RP(), True, [])

        # setup other player commands
        self.setupCommand(Abilities(), False, [Player.PLAYER_MODE_SKATER, Player.PLAYER_MODE_SESSION])
        self.setupCommand(Use(), False, [Player.PLAYER_MODE_SKATER, Player.PLAYER_MODE_SESSION])
        self.setupCommand(Talk(), False, [Player.PLAYER_MODE_SKATER, Player.PLAYER_MODE_SESSION])
        self.setupCommand(Read(), False, [Player.PLAYER_MODE_SKATER, Player.PLAYER_MODE_SESSION])
        self.setupCommand(Trick(), False, [Player.PLAYER_MODE_SKATER, Player.PLAYER_MODE_SESSION])
        self.setupCommand(Tag(), False, [Player.PLAYER_MODE_SKATER, Player.PLAYER_MODE_SESSION])
        self.setupCommand(End(), False, [Player.PLAYER_MODE_SESSION])

        # setup admin/player admin commands
        self.setupCommand(AddArgument(), False, [])
        self.setupCommand(Broadcast(), False, [])
        self.setupCommand(Teleport(), False, [])
        self.setupCommand(Give(), False, [])
        self.setupCommand(Rebuild(), False, [])
        self.setupCommand(Restore(), False, [])
        self.setupCommand(UnlockTrick(), False, [])
        self.setupCommand(Display(), False, [])
        self.setupCommand(SetPlayerFlag(), False, [])
        self.setupCommand(SetPlayerMode(), False, [])
        self.setupCommand(PostTweet(), False, [])
        self.setupCommand(Deploy(), False, [])
        self.setupCommand(Restart(), False, [])
        self.setupCommand(Halt(), False, [])

        # add all the base commands to each mode
        for mode in self.player_modes:
            if mode not in self.mode_commands:
                self.mode_commands[mode] = []

            self.mode_commands[mode].extend(self.base_commands)

        # setup player mode errors for each player mode
        self.mode_errors[Player.PLAYER_MODE_GHOST] = "Ghosts cannot do that..."
        self.mode_errors[Player.PLAYER_MODE_BABY] = "Babies cannot do that..."
        self.mode_errors[Player.PLAYER_MODE_SKELETON] = "Skellies cannot do that..."

    def setupData( self ):
        # clear the PlayerData
        self.data = dict()

        # setup Player Base information
        self.data[Player.PLAYER_DATA_USERNAME] = ""
        self.data[Player.PLAYER_DATA_PASSWORD] = ""
        self.data[Player.PLAYER_DATA_ADMIN] = False
        self.data[Player.PLAYER_DATA_MODE] = Player.PLAYER_MODE_DISCONNECTED

        # load default Player Data
        playerData = json.load(open("_data/player/player.json"))

        # setup Player starting location
        self.data[Player.PLAYER_DATA_LOCATION] = JSONUtility.getData('start', playerData)

        # setup Player Health information
        initialHealth = int(JSONUtility.getData('health', playerData))
        self.data[Player.PLAYER_DATA_HEALTH] = {
            Player.PLAYER_DATA_HEALTH_CURRENT: initialHealth,
            Player.PLAYER_DATA_HEALTH_MAX: initialHealth
        }

        # setup Player Tricks information
        self.data[Player.PLAYER_DATA_TRICKS] = []
        tricksData = JSONUtility.getData('tricks', playerData)
        for trickName in tricksData:
            self.unlockTrick(trickName.upper())

        # initialize Player Stats Information
        self.data[Player.PLAYER_DATA_STATS] = {}
        for statType in self.stat_types:
            stat = dict()
            stat["value"] = 1
            stat["counter"] = 0
            stat["next"] = 3
            self.data["stats"][statType] = stat

        # setup Player Stats Information
        statsData = JSONUtility.getData('stats', playerData)
        for statType in statsData:
            statQuantity = int(statsData[statType])
            statType = statType.upper()
            if (self.hasStat(statType)):
                self.data[Player.PLAYER_DATA_STATS][statType]["value"] = statQuantity
                self.data[Player.PLAYER_DATA_STATS][statType]["next"] = statQuantity + self.data[Player.PLAYER_DATA_STATS][statType]["next"]

        # setup Player Items information
        self.data[Player.PLAYER_DATA_ITEMS] = {}
        itemsData = JSONUtility.getData('items', playerData)
        for itemType in itemsData:
            itemQuantity = int(itemsData[itemType])
            itemType = itemType.upper()
            if (self.itemFactory.getItem(itemType)):
                self.giveItem(itemType, itemQuantity)

        # setup Player Flags information
        self.data[Player.PLAYER_DATA_FLAGS] = {}
        flagData = JSONUtility.getData('flags', playerData)
        for flag in flagData:
            self.setPlayerFlag(flag, flagData[flag])

    def setupUser(self, username, password):
        self.data[Player.PLAYER_DATA_USERNAME] = username
        self.data[Player.PLAYER_DATA_PASSWORD] = password
        self.setPlayerMode(Player.PLAYER_MODE_SKATER)

        self.save()

    def authenticate(self, password):
        return self.data[Player.PLAYER_DATA_PASSWORD] == password

    def completeLogin(self, mode):
        self.setPlayerMode(mode)
        self.save()
    
    def save(self):
        # if we were in a session we should end it before we save out
        self.exitSession()

        self.client.savePlayerData(json.dumps(self.data))
    
    def load(self, username):
        # load and unpickle the player data from Redis
        self.data = json.loads(self.client.loadPlayerData(username))

        # retrieve the last mode the player was in before they logged out so we can return it
        lastMode = self.getPlayerMode()

        # set the player to disconnected so we can complete the login process
        self.setPlayerMode(Player.PLAYER_MODE_DISCONNECTED)

        return lastMode

    def hasStat(self, stat):
        return (stat in self.stat_types)

    def hasItem(self, itemType):
        return (itemType in self.data[Player.PLAYER_DATA_ITEMS])

    def hasFlag(self, flag):
        return (flag in self.getFlags())

    def commandExists( self, command ):
        return command in self.commands

    def isDisconnected(self):
        return (self.getPlayerMode() == Player.PLAYER_MODE_DISCONNECTED)

    def isConnected(self):
        return not (self.getPlayerMode() == Player.PLAYER_MODE_DISCONNECTED)

    def isInSession( self ):
        return ( self.getPlayerMode() == Player.PLAYER_MODE_SESSION )

    def isGhost( self ):
        return ( self.getPlayerMode() == Player.PLAYER_MODE_GHOST )

    def isAdmin( self ):
        return self.data[Player.PLAYER_DATA_ADMIN]

    def getUsername( self ):
        return self.data[Player.PLAYER_DATA_USERNAME]

    def getHealth(self):
        return self.data[Player.PLAYER_DATA_HEALTH]

    def getStat(self, stat):
        if (self.hasStat(stat)):
            return self.data[Player.PLAYER_DATA_STATS][stat]["value"]

        return None

    def getStatCounterValue(self, stat):
        if (self.hasStat(stat)):
            return self.data[Player.PLAYER_DATA_STATS][stat]["counter"]

        return None

    def getStatCounterNextValue(self, stat):
        if (self.hasStat(stat)):
            return self.data[Player.PLAYER_DATA_STATS][stat]["next"]

        return None

    def getFlag(self, flag):
        if (self.hasFlag(flag)):
            return self.data[Player.PLAYER_DATA_FLAGS][flag]

        return None

    def getFlags(self):
        return self.data[Player.PLAYER_DATA_FLAGS]

    def getPlayerMode(self):
        return self.data[Player.PLAYER_DATA_MODE]

    def getUnlockedTricks(self):
        return self.data[Player.PLAYER_DATA_TRICKS]

    def getItems(self):
        return self.data[Player.PLAYER_DATA_ITEMS]

    def getItemQuantity(self, item):
        if (self.hasItem(item)):
            return self.data[Player.PLAYER_DATA_ITEMS][item]

        return None

    def getServer( self ):
        return self.client.server

    def getWorld( self ):
        return self.client.server.getWorld()

    def getCurrentSession( self ):
        return self.sessionController.getCurrentSession()

    def getSessionController( self ):
        return self.sessionController

    def getCurrentLocation(self):
        return self.getWorld().getLocationByShortName(self.data[Player.PLAYER_DATA_LOCATION])

    def getLastCommand( self ):
        return self.lastCommand

    def getCommands( self ):
        return self.mode_commands[ self.getPlayerMode() ]

    def getCommand( self, command ):
        return self.commands[ command ] if self.commandExists( command ) else None

    def getOriginalCommand( self, equivalent ):
        if ( self.commandExists( equivalent ) ): # if we are looking at an original command just return that
            return equivalent

        for command in self.command_equivalents:
            if ( equivalent in self.command_equivalents[ command ] ):
                return command

        return None

    def setPlayerFlag(self, flag, value):
        self.data[Player.PLAYER_DATA_FLAGS][flag] = value

    def setPlayerMode(self, mode):
        if (mode in self.player_modes):
            self.data[Player.PLAYER_DATA_MODE] = mode
            return True

        return False

    def enterSession(self):
        if (not self.isInSession()):
            return self.setPlayerMode(Player.PLAYER_MODE_SESSION)

        return False

    def exitSession(self):
        if (self.isInSession()):
            return self.setPlayerMode(Player.PLAYER_MODE_SKATER)

        return False

    def move(self, locationIndex):
        self.data[Player.PLAYER_DATA_LOCATION] = locationIndex

    def restoreHealth(self, amount=None):
        maxHealth = self.data[Player.PLAYER_DATA_HEALTH][Player.PLAYER_DATA_HEALTH_MAX]
        if (not amount): # if no amount specified fully restore health
            self.data[Player.PLAYER_DATA_HEALTH][Player.PLAYER_DATA_HEALTH_CURRENT] = maxHealth
        else:
            self.data[Player.PLAYER_DATA_HEALTH][Player.PLAYER_DATA_HEALTH_CURRENT] += amount
            if (self.data[Player.PLAYER_DATA_HEALTH][Player.PLAYER_DATA_HEALTH_CURRENT] > maxHealth):
                self.data[Player.PLAYER_DATA_HEALTH][Player.PLAYER_DATA_HEALTH_CURRENT] = maxHealth

    def takeDamage(self, damage):
        self.data[Player.PLAYER_DATA_HEALTH][Player.PLAYER_DATA_HEALTH_CURRENT] -= damage
        if (self.data[Player.PLAYER_DATA_HEALTH][Player.PLAYER_DATA_HEALTH_CURRENT] <= 0):
            self.setPlayerMode(Player.PLAYER_MODE_GHOST)

    def giveItem(self, itemType, quantity = 1):
        item = self.itemFactory.getItem(itemType)
        if (item): # this is a real item type
            if (self.hasItem(itemType)):
                self.data[Player.PLAYER_DATA_ITEMS][itemType] += quantity
            else:
                self.data[Player.PLAYER_DATA_ITEMS][itemType] = quantity

            return True

        return False

    def removeItem(self, itemType, quantity = 1):
        item = self.itemFactory.getItem(itemType)
        if (item): # this is a real item type
            if (self.hasItem(itemType)):
                self.data[Player.PLAYER_DATA_ITEMS][itemType] -= quantity

                # if we have none left then remove the item type
                if (self.data[Player.PLAYER_DATA_ITEMS][itemType] == 0):
                    self.data[Player.PLAYER_DATA_ITEMS].pop(itemType, None)

            return True

        return False

    def unlockTrick(self, trickName):
        if (not trickName in self.data[Player.PLAYER_DATA_TRICKS]):
            self.data[Player.PLAYER_DATA_TRICKS].append(trickName)
            return True

        return False

    def statCheckEntity( self, stat, entity ):
        if ( self.hasStat( stat ) ):
            roll = GameUtility.rollDie( GameUtility.DEFAULT_DIE_SIDES )

            return ( roll + self.getStat( stat ) > entity.getTrickDifficulty( stat ) )
        else:
            return False; 

    def statCheckTrick( self, stat, trick ):
        if ( self.hasStat( stat ) ):
           roll = GameUtility.rollDie( GameUtility.DEFAULT_DIE_SIDES )

           return ( roll + self.getStat( stat ) > trick.difficulty )
        else:
            return False

    def upgradeStat(self, stat, amount=1):
        if (self.getStat(stat)):
            self.data[Player.PLAYER_DATA_STATS][stat]["counter"] += amount
            if (self.data[Player.PLAYER_DATA_STATS][stat]["counter"] >= self.data[Player.PLAYER_DATA_STATS][stat]["next"]):
                self.data[Player.PLAYER_DATA_STATS][stat]["counter"] = 0
                self.data[Player.PLAYER_DATA_STATS][stat]["next"] += self.data[Player.PLAYER_DATA_STATS][stat]["next"]
                self.data[Player.PLAYER_DATA_STATS][stat]["value"] += 1

                return True

        return False

    def performCommand( self, server, command, arguments ):
        if ( command in self.commands ):
            return self.commands[ command ].run( self, server, arguments )
        elif ( self.isAdmin() ):
            return self.performPlayerAdminCommand( server, command, arguments )

    def performAdminCommand( self, server, command, arguments ):
        if ( command.upper() in self.adminCommands ):
            return self.adminCommands[ command.upper() ].run( self, server, arguments )

    def performPlayerAdminCommand( self, server, command, arguments ):
        if ( command.upper() in self.playerAdminCommands ):
            return self.playerAdminCommands[ command.upper() ].run( self, server, arguments )

    def processCommand( self, server, baseCommand, command, arguments ):
        self.lastCommand = ( baseCommand, arguments )

        mode = self.getPlayerMode()
        if ( mode in self.player_modes ):
            commands = self.getCommands()

            if ( baseCommand in commands or ( self.isAdmin() and baseCommand in self.playerAdminCommands ) ):
                return self.performCommand( server, baseCommand, arguments )
            elif ( 'SESSION' in commands ):
                if ( self.isInSession() ):
                    sessionResponse = self.sessionController.advance( command, arguments )
                else:
                    sessionResponse = self.sessionController.oneshot( command, arguments )

                responseCode = sessionResponse.getCode()
                responseMessage = sessionResponse.getMessage()
                if ( responseCode == SessionResponse.SESSION_RESPONSE_CONTINUE ):
                    responseSplit = responseMessage.split( "\n" )
                    if ( len( responseSplit ) > 0 ):
                        if ( sessionResponse.getArgument( "broadcast" ) ):
                            sessionResponse.setArgument( "broadcast", responseSplit[0].replace( "You", self.getUsername() ) + ", " + sessionResponse.getArgument( 'broadcast' ) )
                        else:
                            sessionResponse.addArgument( "broadcast", responseSplit[0].replace( "You", self.getUsername() ) )

                return CommandResponse( self, server, CommandResponse.SEND_TO_PLAYER, sessionResponse.getMessage(), sessionResponse.getArguments() )
            else:
                if ( mode in self.mode_errors ):
                    return CommandResponse( self, server, CommandResponse.SEND_TO_PLAYER, self.mode_errors[ mode ], {} )
                else:
                    return CommandResponse( self, server, CommandResponse.SEND_TO_PLAYER, Player.PLAYER_DEFAULT_ERROR, {} )
        else:
            return CommandResponse( self, server, CommandResponse.SEND_TO_PLAYER, Player.PLAYER_DEFAULT_ERROR, {} )
