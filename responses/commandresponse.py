from response import *
from utility import *

from utilities.scriptingutility import *

class CommandResponse( Response ):
    NO_MESSAGE = -1
    SEND_TO_PLAYER = 1
    SEND_TO_ALL = 2
    LOGIN_SUCCESS = 3
    LOGIN_FAILURE = 4
    LOGOUT = 5
    ADMIN_RESPONSE = 6

    CANNOT_DO_THAT = "CANNOT DO TO THAT!"

    USERNAME_CODE = "[USERNAME]"

    def __init__( self, player, server, code, message, arguments=None ):
        super( CommandResponse, self ).__init__( code, message, arguments )

        self.code = code
        self.message = ScriptingUtility.applyTags( message, player )
        self.arguments = arguments
        if ( not self.arguments ):
            self.arguments = {}

        if "broadcast" in self.arguments:
            self.arguments[ "broadcast" ] = ScriptingUtility.applyTags( self.arguments[ "broadcast" ], player )
