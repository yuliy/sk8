from response import *

class SessionResponse( Response ):
    SESSION_RESPONSE_FAILURE = 0
    SESSION_RESPONSE_CONTINUE = 1
    SESSION_RESPONSE_STALL = 2

    def __init__( self, code, message, arguments=None ):
        super( SessionResponse, self ).__init__( code, message, arguments )
            