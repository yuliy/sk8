from response import *

class ActionResponse( Response ):
    ACTION_RESPONSE_FAILURE = 0
    ACTION_RESPONSE_SUCCESS = 1

    DEFAULT_SUCCESS_MESSAGE = "YOU DID THAT PERFECTLY!"
    DEFAULT_FAILURE_MESSAGE = "YOU FAILED TO DO THAT!"

    NO_ITEM_FAILURE_MESSAGE = "DON'T HAVE REQUIRED ITEMS TO DO THAT!"

    def __init__( self, code, message, arguments=None ):
        super( ActionResponse, self ).__init__( code, message, arguments )
        