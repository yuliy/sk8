from __future__ import with_statement

import glob
import json
import os
import re

from fabric.api import *

TARGET_BACKEND = "backend"
TARGET_FRONTEND = "frontend"


def get_build_config_file():
    """
    Utility function to retrieve the file path to the build configuration file.

    :return:
    """
    # find the build config file path
    current_path = os.path.dirname(os.path.abspath(__file__))
    config_file = current_path + "/../_data/config/build.json"

    return config_file


def get_deployment_config(deployment, config_file=None):
    """
    Utility function to retrieve the build configuration for a certain deployment.  The format of the configuration
    file will be:

    {
        "deployment": {
            "target": {
                "host": "USER@HOST_NAME",
                "service": "SERVICE_NAME"
            },
            ...
        },
        ...
    }

    :param deployment:
    :param config_file:
    :return:
    """
    if config_file is None:
        config_file = get_build_config_file()

    config = json.load(open(config_file))
    deployment_config = config[deployment]
    return deployment_config


def setup(target, deployment):
    """
    Fabric Task to setup the Fabric env before running deployment tasks from the command line interface.  Example
    usage:

    fab -f build/fabfile.py setup:backend,dev

    will setup the Fabric env to point to the "dev" deployment "backend" server as defined in the configuration file
    "_data/config/build.json".
    :param target:
    :param deployment:
    :return:
    """

    deployment_config = get_deployment_config(deployment)
    target_host = deployment_config[target]["host"]
    env.hosts = [target_host]


def deploy(branch, deployment, target, deployment_config=None):
    """
    Fabric task to deploy some branch to a part of some specified deployment.  Example CLI usage:

    fab -f build/fabfile.py setup:backend,dev,_data/config/build.json deploy:develop,dev,backend

    Note: We need to run the Setup Fabric task before deploy so that the Fabric env is appropriately setup
    for use during the deploy task.

    :param branch:
    :param deployment:
    :param target:
    :param deployment_config:
    :return:
    """
    # load the deployment configuration if it was not specified
    if deployment_config is None:
        deployment_config = get_deployment_config(deployment)

    target_config = deployment_config[target]

    code_dir = '/home/sk8/'
    with cd(code_dir):
        if target_config["git_update"]:
            run("git reset HEAD --hard")

            run("git fetch origin")

            run("git checkout " + branch)

            run("git pull origin " + branch)

        if target_config["pip_install"]:
            sudo("pip install -r _data/config/requirements.txt")

    if target_config["harp_config"] is not False:
        client_dir = '/home/sk8/frontend/client'
        with cd(client_dir):
            current_path = os.path.dirname(os.path.abspath(__file__))
            put(current_path + "/../_data/config/harp/" + target_config["harp_config"], "_harp.json")

    if target_config["service"] is not False:
        run("service " + target_config["service"] + " restart")


def deploy_admin(branch):
    """
    Fabric task to setup the Admin server.  This task will pull from a given branch and then restart the sk8mud
    application service and the sk8mud frontend admin service.  Example CLI usage:

    fab -f build/fabfile.py setup:backend,admin,_data/config/build.json deploy_admin:develop

    Note: We need to run the Setup Fabric task before deploy_admin so that the Fabric env is appropriately setup
    for use during the deploy_admin task.

    :param branch:
    :return:
    """

    # 1. Deploy Admin Backend Service
    deploy(branch, "admin", TARGET_BACKEND)

    # 2. Deploy Admin Frontend Service
    deploy(branch, "admin", TARGET_FRONTEND)

    # 3. Deploy build configurations
    build_config_dir = '/home/sk8/_data/config'
    with cd(build_config_dir):
        # Add/update the build.json files for update deployment configurations on the admin
        put(get_build_config_file(), "build.json")

    # 4. Deploy harp configurations (used when deploying dev/prod from the admin)
    harp_config_dir = '/home/sk8/_data/config/harp'
    with cd(harp_config_dir):
        # Add/update the harp.json files for deploying admin or client instances of harp from the admin
        current_path = os.path.dirname(os.path.abspath(__file__))
        for file_path in glob.iglob(current_path + "/../_data/config/harp/*.json"):
            file_name = re.findall("[a-zA-Z]*.json", file_path)
            file_name = file_name[0]  # just take the first thing we find which matches *.json
            put(file_path, file_name)


def restart(deployment, target, deployment_config=None):
    """
    Fabric Task to restart a frontend/backend service on some deployment based on some deployment configuration.

    :param deployment:
    :param target:
    :param deployment_config:
    :return:
    """
    # load the deployment configuration if it was not specified
    if deployment_config is None:
        deployment_config = get_deployment_config(deployment)

    run("service " + deployment_config[target]["service"] + " restart")


def halt(deployment, target, deployment_config=None):
    """
    Fabric Task to halt a frontend/backend service on some deployment based on some deployment configuration.

    :param deployment:
    :param target:
    :param deployment_config:
    :return:
    """
    # load the deployment configuration if it was not specified
    if deployment_config is None:
        deployment_config = get_deployment_config(deployment)

    service = deployment_config[target]["service"]

    initctl_status = run("initctl status " + service)

    # make sure that the service is not already stopped
    if "stop" not in str(initctl_status):
        run("service " + service + " stop")
