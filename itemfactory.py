import json

from xml.dom.minidom import *

from item import *

class ItemFactory( object ):
    def __init__( self ):
        super( ItemFactory, self ).__init__()

        self.definitions = {}
        self.nameToShortName = {}

        itemDefinitions = json.load(open("_data/item/items.json"))
        for itemName in itemDefinitions:
            self.setupItem(itemName, itemDefinitions[itemName])

    def setupItem( self, itemName, itemData ):
        item = Item( itemName, itemData )
        self.addDefinition( item )

    def addDefinition( self, item ):
        self.definitions[ item.shortName.upper() ] = item
        self.nameToShortName[ item.entityType ] = item.shortName

    def getItem( self, shortName ):
        if ( shortName in self.definitions ):
            return self.definitions[ shortName ]
        elif ( self.getItemShortName( shortName ) in self.definitions ):
            return self.definitions[ self.getItemShortName( shortName ) ]
        else:
            splitShortName = shortName.split()
            if ( len( splitShortName ) > 0 ):
                if ( splitShortName[0] in GameUtility.ARTICLES ):
                    shortName = ' '.join( shortName.split()[1:] )
                    return self.getItem( shortName )

        return None

    def getItemShortName( self, name ):
        if ( name in self.nameToShortName ):
            return self.nameToShortName[ name ]

        return None
        